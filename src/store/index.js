import { configureStore } from "@reduxjs/toolkit";
import { setupListeners } from "@reduxjs/toolkit/query";

import users from "../services/users";
import events from "../services/events";
import ticketTypes from "../services/ticketTypes";

const store = configureStore({
  reducer: {
    [events.reducerPath]: events.reducer,
    [ticketTypes.reducerPath]: ticketTypes.reducer,
    [users.reducerPath]: users.reducer,
  },

  middleware: (getDefaultMiddleware) =>
    getDefaultMiddleware() 
      .concat(events.middleware) 
      .concat(ticketTypes.middleware)
      .concat(users.middleware),

  devTools: process.env.NODE_ENV !== "production",
});

setupListeners(store.dispatch);

export default store;
