import { TICKET_DETAILS } from "./constants";

const initialReservation = []

export const ticketDetailsReducer = (state = initialReservation, action) => {
  switch (action.type) {
    case TICKET_DETAILS:
      {
      return action.payload;
      }
    default:
      return state;
  }
};
