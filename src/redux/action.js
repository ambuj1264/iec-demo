import { TICKET_DETAILS } from "./constants"

export const ticketDetails = (data)=>{
    return {
      type: TICKET_DETAILS,
      payload: data
    }
}