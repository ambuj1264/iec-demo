import { combineReducers } from "redux";
import {ticketDetailsReducer} from "./reducer"
const rootReducer = combineReducers({
  ticketDetailsReducer, // Rename the reducer key if needed

});

export default rootReducer;
