import { createApi, fetchBaseQuery } from "@reduxjs/toolkit/query/react";

const events = createApi({
  reducerPath: "events",

  baseQuery: fetchBaseQuery({baseUrl: process.env.NEXT_PUBLIC_API}),
  tagTypes: ["Event"],

  endpoints: (builder) => ({
    listEvent: builder.query({
      query: (code) => `/api/events`,
      providesTags: ["Event"],
    }),

    eventDetails: builder.query({
      query: (code) => `/api/events/${code}`,
      providesTags: ["Event"],
    }),
 
  }),
});

export const {
  useListEventQuery,
  useEventDetailsQuery,
} = events;

export default events;
