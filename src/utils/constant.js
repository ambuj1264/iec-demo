import Yourself from "../components/eventbooking/tabsStep/Yourself";

export const tabsData = [
    { label: 'Yourself', content: <Yourself/> },
    { label: 'Elderly 1', content: <p>two!</p> },
    { label: 'Elderly 2', content: <p>three!</p> },
    { label: 'Elderly 3', content: <p>Four!</p> },
];

export const countryoptions = [
    { value: 'India', label: 'India' },
    { value: 'Uk', label: 'UK' },
    { value: 'USA', label: 'USA' },
];

export const tickettypeoptions = ['Bronze', 'Silver', 'Gold']

export const  eventbookingsteps = [
    { title: 'Date & Time', description: 'Contact Info' },
    { title: 'Quantity & Tickets', description: 'Date & Time' },
    { title: 'Register', description: 'Select Rooms' },
];
