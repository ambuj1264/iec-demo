import * as Yup from 'yup';

export const validationSchema = Yup.object().shape({
    name: Yup.string().required('Name is required'),
    email: Yup.string().email('Invalid email address').required('Email is required'),
    country: Yup.string(),
    number: Yup.string()
    .matches(/^[0-9]{10}$/, 'Phone number must be 10 digits')
    .required('Phone number is required'),
    ticketType: Yup.string()
  });
export const validationForNumber = Yup.object().shape({
  number: Yup.string()
  .matches(/^[0-9]{10}$/, 'Phone number must be 10 digits')
  .required('Phone number is required')
  });

export const validateStep = (loginStep,isEmail) => {
  switch (loginStep) {
    case 0:
      return Yup.object().shape({
        number: Yup.string()
          .matches(/^[0-9]{10}$/, 'Phone number must be 10 digits')
          .required('Phone number is required'),
        code: Yup.string().required('Country code is required'),
      });
    case 1:
      if (isEmail) {
        return Yup.object().shape({
          email: Yup.string()
            .email('Invalid email address')
            .required('Email is required'),
        });
      } else {
        return Yup.object().shape({
          number: Yup.string()
            .matches(/^[0-9]{10}$/, 'Phone number must be 10 digits')
            .required('Phone number is required'),
          code: Yup.string().required('Country code is required'),
        });
      }
    case 2:
        return Yup.object().shape({
            otp1: Yup.string()
              .required('This field is required'),
              otp2: Yup.string()
              .required('This field is required'),
              otp3: Yup.string()
              .required('This field is required'),
              otp4: Yup.string()
              .required('This field is required'),
            });
    case 3:
      if (isEmail) {
        return Yup.object().shape({
          email: Yup.string()
            .email('Invalid email address')
            .required('Email is required'),
          name:  Yup.string()
          .required('This field is required'),
        });
      } else {
        return Yup.object().shape({
          number: Yup.string()
            .matches(/^[0-9]{10}$/, 'Phone number must be 10 digits')
            .required('Phone number is required'),
          code: Yup.string().required('Country code is required'),
          name: Yup.string()
          .required('This field is required'),
        });
      }
    default:
      return Yup.object();
  }
};