import * as moment from 'moment';

export const slugifyUrl = (title) => {
    return title.toLowerCase().replace(/ /g, '-')
    .replace(/[^\w-]+/g, '');
}

export const getUniqueDates = (data) => {
    const uniqueDates = []; 
    
    data.map(date =>  {
        const dateString = moment(date?.startDate).format('dddd DD MMM, YYYY'); 
        if(uniqueDates.indexOf(dateString) < 0) {
            uniqueDates.push(dateString)
        }
    })
    return uniqueDates;
}