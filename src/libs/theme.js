import {
    extendTheme,
    withDefaultProps,
    withDefaultSize,
    withDefaultVariant,
  } from "@chakra-ui/react";
  
  const variantOutlined = () => ({
    field: {
      _focus: {
        borderColor: "var(--chakra-ui-focus-ring-color)",
        boxShadow: "0 0 0 2px var(--chakra-ui-focus-ring-color)",
      },
    },
  });
  
  const theme = extendTheme(
    {
      initialColorMode: "light",
      useSystemColorMode: false,
      styles: {
        global: {
          body: {
            bg: "#fff",
            color: "#000",
            fontFamily: "'inter'",
            fontSize: "14px",
          },
          "::-moz-selection": {
            color: "white",
            background: "var(--chakra-colors-blue-600)",
          },
          "::selection": {
            color: "white",
            background: "var(--chakra-colors-blue-600)",
          },
          ":host,:root": {
            "--chakra-ui-focus-ring-color": "var(--chakra-colors-blue-600)",
          },
          shadows: { outline: "0 0 0 3px var(--chakra-ui-focus-ring-color)" },
        },
      },
      components: {
        Heading: {
          baseStyle: {
            fontFamily: "inter",
          },
        },
        Button: {
          baseStyle: {
            rounded: "full",
          },
        },
        Input: {
          variants: {
            outline: variantOutlined,
          },
        },
        Select: {
          variants: {
            outline: variantOutlined,
          },
        },
        Textarea: {
          variants: {
            outline: () => variantOutlined().field,
          },
        },
      },
    },
    withDefaultProps({
      defaultProps: { colorScheme: "blue", rounded: "full" },
    }),
    withDefaultVariant({
      variant: "outline",
      components: ["Button"],
    }),
    withDefaultSize({
      size: "sm",
      components: ["Button"],
    })
  );
  
  export default theme;
  