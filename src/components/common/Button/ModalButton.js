import React from 'react';
import styled from "styled-components";

const ModalButton = ({ bg }) => {
  return (
    <StyledButton style={{ backgroundColor: bg }}>CONTINUE</StyledButton>
  );
};

export default ModalButton;

const StyledButton = styled.button`
  width: 344px;
  height: 36px;
  color: #999999;
  position: absolute;
    bottom: 10px;
    left:16px !important;
  background-color:#EEEEEE;
  border-radius: 4px;
  font-family: Inter;
font-size: 12px;
font-weight: 700;
line-height: 15px;
text-align: center;

`;
