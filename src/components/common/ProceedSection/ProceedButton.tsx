import React from 'react';
import styled from 'styled-components';

const ProceedButton = ({color}) => {
  return (
    <>
    <MainDiv>
    <LongButton>
      <ButtonImage
        loading="lazy"
        src="https://cdn.builder.io/api/v1/image/assets/TEMP/c6c7256ad66751c23c4c38e42efe3ea08fbf40e221c47880bf1df5f7a3fba841?apiKey=9aef8ecf740c47caa7855ca5ffbd8b31&"
      />
       PROCEED
    </LongButton>
  </MainDiv>
  </>
  )
}

export default ProceedButton;

const MainDiv = styled.div`
  box-shadow: 0px -2px 4px 0px rgba(0, 0, 0, 0.08);
  background-color: #fff;
  align-self: stretch;
  display: flex;
  margin-top: 25px;
  width: 100%;
  flex-direction: column;
  justify-content: center;
  align-items: center;
  padding: 16px 60px;
  @media (max-width: 991px) {
    max-width: 100%;
    padding: 0 20px;
  }
`;

const LongButton = styled.div`
  disply: flex;
  flex-direction: column;
  color: #fff;
  text-align:center;
  text-transform: capitalize;
  position: relative;
  white-space: nowrap;
  fill: var(--CTA, #0a84ff);
  overflow: hidden;
  min-height: 48px;
  width: 776px;
  max-width: 100%;
  justify-content: center;
  align-items: center;
  padding: 18px 60px;
  font: 700 17px Inter, sans-serif;
  z-index:111;
  
  @media (max-width: 991px) {
    white-space: initial;
    padding: 0 20px;
  }
`;

const ButtonImage = styled.img`
  position: absolute;
  inset: 0;
  height: 100%;
  width: 100%;
  object-fit: cover;
  object-position: center;
  z-index:-22
`;