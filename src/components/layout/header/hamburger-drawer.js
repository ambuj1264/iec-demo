import React from "react";
import Image from "next/image";
import NextLink from "next/link";
import { useRouter } from "next/router";
import {
  IconButton,
  Drawer,
  Stack,
  Link,
  Icon,
  Spacer,
  Text,
  DrawerBody,
  DrawerFooter,
  DrawerHeader,
  DrawerOverlay,
  DrawerContent,
  DrawerCloseButton,
  useDisclosure,
} from "@chakra-ui/react";
import { CloseIcon, HamburgerIcon } from "@chakra-ui/icons";
import { FaBox, FaChalkboardTeacher, FaRocket, FaBook } from 'react-icons/fa'
import { BsFillPeopleFill, BsFillEnvelopePaperFill } from 'react-icons/bs'

const MenuDrawer = () => {
  const { isOpen, onToggle, onClose } = useDisclosure();
  const btnRef = React.useRef();

  return (
    <>
      <IconButton
        color="black.200"
        hover="gray.700"
        onClick={onToggle}
        icon={
          isOpen ? <CloseIcon w={3} h={3} /> : <HamburgerIcon w={8} h={8} />
        }
        variant={"ghost"}
        aria-label={"Toggle Navigation"}
      />
      <Drawer
        isOpen={isOpen}
        placement="left"
        onClose={onClose}
        finalFocusRef={btnRef}
      >
        <DrawerOverlay />
        <DrawerContent width={'4xl'} alignContent={'center'}>
          <DrawerCloseButton />
          <DrawerHeader>
            <Image alt="IEC" src='/images/header/logo.png' width={150} height={10} />
          </DrawerHeader>

          <DrawerBody>
            <Stack>
              <Link my={5} as={NextLink} href={'/why-iec'}> 
                <Text as={"span"} verticalAlign={'center'} fontSize={15}></Text>Why IEC?
              </Link>
              
              <Link my={'5 !important'} as={NextLink} href={'/events'}> 
                <Text as={"span"} verticalAlign={'center'} fontSize={15}>
                  Events
                </Text>
              </Link>
              <Link my={'5 !important'} as={NextLink} href={'/nursing'}> 
                <Text as={"span"} verticalAlign={'center'} fontSize={15}>Nursing</Text>
              </Link>
              <Link my={'5 !important'} as={NextLink} href={'/'}> 
                <Text as={"span"} verticalAlign={'center'} fontSize={15}>Sr. Care Packages</Text>
              </Link>
              <Link my={'5 !important'} as={NextLink} href={'/'}> 
                <Text as={"span"} verticalAlign={'center'} fontSize={15}>Celebrities</Text>
              </Link>
              {/* <Link my={'5 !important'}  href={'/catalogue'}>
                <Icon as={FaBook} width={'20px'} marginInlineEnd={3}/> 
                <Text as={"span"} verticalAlign={'center'} fontSize={15}>Catalogue</Text>
              </Link> */}
              <Link my={'5 !important'} as={NextLink} href={'/search'}> 
                <Text as={"span"} verticalAlign={'center'} fontSize={15}>Search</Text>
              </Link>
              
            </Stack>
          </DrawerBody>
        </DrawerContent>
      </Drawer>
    </>
  );
};

export default MenuDrawer;
