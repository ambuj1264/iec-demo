import {
  Box,
  Flex,
  IconButton,
  Button,
  Stack,
  Collapse,
  Icon,
  Link,
  Text,
  Input,
  Popover,
  PopoverTrigger,
  PopoverContent,
  useColorModeValue,
  useBreakpointValue,
  useDisclosure,
  Image,
  Modal,
  ModalOverlay,
  ModalContent,
  ModalHeader,
  ModalFooter,
  ModalBody,
  ModalCloseButton,
  chakra,
  HStack,
  VStack,
  CloseButton,
  VisuallyHidden,
  InputGroup,
  InputLeftElement,
  Avatar,
  Hide,
} from '@chakra-ui/react';
// import { Logo } from "@choc-ui/logo";
import { useSession } from 'next-auth/react';
import {
  AiOutlineMenu,
  AiFillHome,
  AiOutlineInbox,
  AiOutlineSearch,
  AiFillBell,
} from "react-icons/ai";
import { BsFillCameraVideoFill } from "react-icons/bs";
import {
  HamburgerIcon,
  CloseIcon,
  ChevronDownIcon,
  ChevronRightIcon,
} from '@chakra-ui/icons';
import { Router, useRouter } from 'next/router'
import { BiCurrentLocation } from 'react-icons/bi'
import React, { useState, useEffect } from 'react';
import mapboxSdk from '@mapbox/mapbox-sdk/services/geocoding';
import MenuDrawer from './hamburger-drawer'

const Header = ({showMenu}) => {
  const bg = useColorModeValue("white", "gray.800");
  const mobileNav = useDisclosure();
  const { isOpen, onToggle } = useDisclosure();
  const { isOpen: isModalOpen, onOpen: onModalOpen, onClose } = useDisclosure(); 
  const router = useRouter();
  const { data: session, status } = useSession() as any;
  
 
  return (
    <HStack display={{md:'flex'}} justifyContent={'center'}>
      <chakra.header
        backgroundColor={'#fff'}
        // w={{md:"7xl"}}
        w={'100%'}
        paddingLeft={50}
        paddingRight={50}
        // px={{
        //   base: 2,
        //   sm: 4,
        // }}
        paddingTop={{md: 4, base: 2}}
        // shadow="md"
      >
        <Flex alignItems="center" justifyContent="space-between" mx="auto">
          <HStack display="flex" spacing={3} alignItems="center" color={'#333'} fontWeight={'500'} fontSize={'16px'}>
            <Box
              display={{
                base: "inline-flex",
                md: "none",
              }}
            >
              <MenuDrawer />
               
            </Box>
            <chakra.a
              href="/"
              title="Choc Home Page"
              display="flex"
              alignItems="center"
              paddingRight={'30px'}
            >
              <Image alt="IEC" src='/images/header/logo.png' />
              <VisuallyHidden>Choc</VisuallyHidden>
            </chakra.a>
             {showMenu && 
              <Hide below='md'>
             
                <chakra.a
                  href="/events"
                  title="Events"
                  display="flex"
                  alignItems="center"
                  paddingInline={'12px'}
                >
                  Events
                </chakra.a>
                
                <chakra.a
                  href="/nursing"
                  title="Nursing"
                  display="flex"
                  alignItems="center"
                  paddingInline={'12px'}
                >
                  Nursing
                </chakra.a>
                <chakra.a
                  href="/#"
                  title="Sr. Care Packages"
                  display="flex"
                  alignItems="center"
                  paddingInline={'12px'}  
                >
                  Sr. Care Packages
                </chakra.a>
                <chakra.a
                  href="/#"
                  title="Celebrity"
                  display="flex"
                  alignItems="center"
                  paddingInline={'12px'}  
                >
                  Celebrity
                </chakra.a>
              
                
            </Hide>
            }
          </HStack>
         
          <HStack
            spacing={3}
            display={mobileNav.isOpen ? "none" : "flex"}
            alignItems="center"
          >
             {showMenu && 
            <Hide below='md'>
              <InputGroup>
                <InputLeftElement pointerEvents="none">
                  <AiOutlineSearch />
                </InputLeftElement>
                <Input type="tel" placeholder="Search..." />
              </InputGroup>
            </Hide>
            
          }

            {session &&
              <Button as={'a'} href="#"  >
                <Avatar
                  size="md"
                  name="Dan Abrahmov"
                  src="https://bit.ly/dan-abramov"
                />
              </Button>

            }
            {!session &&
            <>
              <Hide below='md'>
                <Button paddingBlock={5} paddingInline={10} onClick={() => router.replace('/signup')}   borderColor={'#0A84FF'} color={'#333'} variant={'outline'}> Sign Me Up </Button>
                <Button paddingBlock={5} paddingInline={10} onClick={() => router.replace('/login')}   backgroundColor={'#0A84FF'} variant={'solid'}> Log In </Button>
              </Hide>
            </>
            }

          </HStack>
        </Flex>
        <Flex height={1} marginBlock={5} flexDirection={'row'}>
            <Box  background={'#F49AC1'}  flex={1}/>
            <Box  background={'#FFC20E'}  flex={1}/>
            <Box  background={'#28BCA5'}  flex={2}/>
        </Flex>
      </chakra.header>
       
    </HStack>
  );
};

export default Header;