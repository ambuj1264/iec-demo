import React, { useEffect, useState } from 'react';
import styled from 'styled-components';
import { EmailIcon, GoogleIcon, LoginHeaderIcon } from '../../../utils/SVGIcons/SVG';
import { Box, Button, Text, VStack } from '@chakra-ui/react';
import Link from 'next/link';
import NumberWithCode from '../../list/ui/input/NumberWithCode';

const StyledLoginContainer = styled(VStack)`
  padding: 10px;
  margin-top: 10px;

  @media (min-width: 48em) {
    padding: 20px;
    margin-top: 20px;
  }
`;

const StyledText = styled(Text)`
  font-size: 20px;
  font-weight: 700;
  text-transform: capitalize;
  margin-top: 5px;

  @media (min-width: 48em) {
    font-size: 20px;
  }
`;

const StyledParaText = styled(Text)`
  color: #666;
  text-align: center;
  font-family: Inter;
  font-size: 12px;
  font-weight: 500;
  line-height: 16px;
  margin: 15px;
  max-width: 296px;

  @media (min-width: 48em) {
    font-size: 12px;
  }
`;

const StyledButton = styled(Button)`
  display: flex !important;
  justify-content: start !important;
  align-items: center !important;
  color: #333333 !important;
  font-size: 14px !important;
  font-weight: 500 !important;
  border-radius: 0 !important;
  border: 1px solid #eee !important;
  width: 100% !important;
  padding: ${({ theme }) => theme.px};
  margin-bottom: 15px !important;

  @media (min-width: 48em) {
    padding: ${({ theme }) => theme.pxMd};
  }
`;

const StyledOrText = styled(Text)`
  color: #666;
  font-size: 14px;
  font-weight: 500;
  text-align: center;
  margin: 5px 0;
`;

const InitialStep = ({ onLoginStep, formikProps }) => {
  const { values, errors, handleChange } = formikProps;
  const noErrors = Object.keys(errors).length === 0;
  const [num, setNum] = useState("")
  const handleNumber = (e) => {
    handleChange(e);
    const { value, name } = e.target;
    if (name === "number") {
      setNum(value)
    }
  }
  useEffect(() => {
    if (noErrors) {
      if (num.length === 10) {
        onLoginStep("Number")
      }
    }
  }, [noErrors, num]);

  console.log(noErrors, values, errors, num)

  return (
    <StyledLoginContainer py={{ base: "10px", md: "20px" }} mt={{ base: "10px", md: "20px" }} align="center">
      <LoginHeaderIcon />
      <StyledText>Login with I Elderly Care</StyledText>
      <StyledParaText>Sign in to purchase tickets, save your favourite events, Book a Nurse and much more. All you need for taking care of your Elderly</StyledParaText>
      <Box width='100%' my={{ base: "5px", md: "10px" }}>
        <StyledButton px={{ base: "15px", md: "25px" }} py="25px">
          <GoogleIcon />
          <Text width='100%'>Continue with Google</Text>
        </StyledButton>
        <StyledButton onClick={() => { onLoginStep("Email") }} px={{ base: "15px", md: "25px" }} py="25px">
          <EmailIcon />
          <Text width='100%'>Continue with Email</Text>
        </StyledButton>
      </Box>
      <StyledOrText>Or</StyledOrText>
      <NumberWithCode values={values} errors={errors} handleChange={handleNumber} />
      <Box>
        <Text style={{ fontSize: "12px", fontWeight: 400, lineHeight: "19px", textAlign: "center", color: "#666" }}>
          I agree to the <br />
          <Link style={{ textDecoration: "underline" }} href={"/"}>Terms & Conditions </Link> and <Link style={{ textDecoration: "underline" }} href={"/"}>Privacy Policy</Link>
        </Text>
      </Box>
    </StyledLoginContainer>
  );
}

export default InitialStep;
