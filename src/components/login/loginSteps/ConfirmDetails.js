import React from 'react'
import styled from 'styled-components';
import { FooterComponent, HeaderComponent } from '../Login';
import InputField from '../../list/ui/input/InputField';
import { inputStyle } from '../../eventbooking/tabsStep/Yourself';
import Link from 'next/link';
import NumberWithCode from '../../list/ui/input/NumberWithCode';

const ConfirmDetails = ({ onLoginStep, formikProps, isEmail }) => {
    const { values, errors,isValid, handleChange } = formikProps
    return (
        <Wrapper>
            <HeaderComponent title="Confirm details" />
            <InputContainer>
                <InputField label="Name" value={values.name} helperText={errors.name} onChange={handleChange} style={inputStyle} type="text" placeholder="Enter Name" name="name" />
            </InputContainer>
            <InputContainer>
                {isEmail ? <InputField label="Email Address" value={values.email} helperText={errors.email} onChange={handleChange} style={inputStyle} type="email" placeholder="Enter Email Address" name="email" />
                    : <NumberWithCode values={values} errors={errors} handleChange={handleChange} isSelect />}
            </InputContainer>
            <StyledLink href="/">Terms & Conditions</StyledLink>
            <FooterComponent isValid={isValid} text="NEXT" onclick={onLoginStep} />
        </Wrapper>)
}

export default ConfirmDetails;

const Wrapper = styled.div`

`
const InputContainer = styled.div`
 margin-bottom:20px
`

const StyledLink = styled(Link)`
 text-decoration:underline;
 text-align:end;
 display:block;
`