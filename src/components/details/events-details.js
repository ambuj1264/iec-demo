import React, {useState} from 'react';
import { useRouter } from 'next/router';
import {
  SimpleGrid,
  VStack,
  Heading,
  Text,
  Box,  
  Image,
  Badge,
  Tabs,
  TabList,
  TabPanel,
  TabPanels,
  Tab,
  Link, 
  GridItem,
  Icon
} from '@chakra-ui/react';
import { BiCalendarAlt, BiCategoryAlt, BiLocationPlus } from 'react-icons/bi'
import { useEventDetailsQuery } from '../../services/events'
import * as moment from 'moment';
import { BsPerson } from 'react-icons/bs';

const EventDetails = () => {
    const router = useRouter();
    console.log('router', router)
    const eventCode = router?.query?.code
    const {
        isLoading,
        data: event,
        isError,
    } = useEventDetailsQuery(eventCode);
   
    const [tabIndex, setTabIndex] = useState(0);
    const dateString = moment(event?.startDateTime).format('ddd DD, MMM')
    const timeString = moment(event?.startDateTime).format('HH:mm A')
    return <Box>
        <VStack maxWidth={'7xl'} alignItems={'flex-start'}>
            <SimpleGrid  columns={{md: 2, base:1}} alignItems={'flex-start'} backdropBlur={'base'} gap={10} flexDirection={'row'}>
                    <GridItem>
                        <Image src={event?.mainBannerUrl} />
                    </GridItem>
                    <GridItem>
                        <Box backgroundColor={'#fff'} borderRadius={20} padding={4}>
                            <Heading as={'h1'}>{event?.name}</Heading>
                            <Box py={5}>
                                {event?.genere && event?.genere.map(genere => {
                                return <Badge rounded="3" fontSize={'10px'} px={3} py={1} colorScheme="teal">
                                            {genere.value}
                                        </Badge>
                                })}
                            </Box>
                            <Box flexDirection={'row'} display={'flex'} gap={3} color={'#666'} mb={5}>
                                <Icon as={BiCategoryAlt} width={5} height={5}/> 
                                <Text>{event?.category}</Text>
                            </Box>
                            <Box flexDirection={'row'} display={'flex'} gap={3} color={'#666'}  mb={5}>
                                <Icon as={BiCalendarAlt} width={5} height={5}/> 
                                <Text>{dateString} | {timeString}</Text>
                            </Box>
                            <Box flexDirection={'row'} display={'flex'} gap={3} color={'#666'}  mb={5}>
                                <Icon as={BiLocationPlus} width={5} height={5}/> 
                                <Text>Google Meet</Text>
                            </Box>
                            <Box flexDirection={'row'} display={'flex'} gap={3} color={'#666'}  mb={5}>
                                <Icon as={BsPerson} width={5} height={5}/> 
                                <Text>For Age 30+</Text>
                            </Box>
                            <Box flexDirection={'row'} display={'flex'} gap={3} color={'#666'}  mb={5}>
                                <Text flex={2} paddingBlock={'20px'}>Free</Text> 
                                <Box display={'flex'} justifySelf={'flex-end'} flexDirection={'row'}  background={'#0A84FF'} borderRadius={3} padding={3} maxWidth={'150px'} textAlign={'center'} color={'#fff'} mb={5}>
                                    <Link textDecor={'none'} href={`/events/booking/${event?.code}`}>
                                        Book Now
                                    </Link>
                                </Box>
                            </Box>
                       
                        </Box>
                    </GridItem>
                    
            </SimpleGrid>

            <SimpleGrid columns={{md: 2, base: 1}} gridTemplateColumns={{md: '2fr 1fr'}}  alignItems={'flex-start'} backdropBlur={'base'} gap={10} marginTop={'28px'} flexDirection={'row'}>
                <GridItem>
                    <Tabs onChange={(index) => setTabIndex(index)}>
                        <TabList>
                            <Tab>About Event</Tab>
                            <Tab>Organizer</Tab> 
                        </TabList>
                        <TabPanels>
                            <TabPanel>
                                <Box dangerouslySetInnerHTML={{__html: event?.description}}/>
                                { event?.termsAndCondition && 
                                <Box>
                                    <Heading fontSize={20} marginBlock={'24px'}>Terms And Conditions</Heading>
                                    <Box dangerouslySetInnerHTML={{__html: event?.termsAndCondition}}/>
                                </Box>
                                }
                            </TabPanel>
                            <TabPanel>Coming Soon</TabPanel> 
                        </TabPanels>
                    </Tabs>
                </GridItem>
                <GridItem display={'flex'} flex={1}>
                    
                </GridItem>
                
            </SimpleGrid>
        </VStack>
    </Box>
}

export default EventDetails;