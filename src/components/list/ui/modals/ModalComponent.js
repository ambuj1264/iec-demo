// CommonModal.js
import React from 'react';
import { Modal, ModalOverlay, ModalContent, ModalHeader, ModalCloseButton, ModalBody, ModalFooter } from '@chakra-ui/react';

const ModalComponent = ({ isOpen, onClose, children, headerComponent, footerComponent, isCancelIcon = true }) => {
  return (
    <Modal isOpen={isOpen} onClose={onClose} size="md">
      <ModalOverlay />
      <ModalContent>
        {headerComponent && <ModalHeader>{headerComponent}</ModalHeader>}
        <ModalCloseButton />
        <ModalBody>{children}</ModalBody>
        {footerComponent && <ModalFooter>
          {footerComponent}
        </ModalFooter>}
      </ModalContent>
    </Modal>
  );
};

export default ModalComponent;
