import {
    Box,
    Stepper,
    Step,
    StepIndicator,
    StepStatus,
    StepIcon,
    StepNumber,
    StepTitle,
    StepSeparator,
    useSteps,
    Flex,
} from '@chakra-ui/react';
import React, { useState } from 'react';
import Step1 from '../../../eventbooking/steps/Steps1';

function CommonStepper({steps}) {
    // const { activeStep } = useSteps({
    //     index: 2,
    //     count: steps.length,
    // })
    const [activeStep, setActiveStep] = useState(2)
    console.log(activeStep,"activeStep")
    

    return (
        <>
            <Stepper index={activeStep} style={steperStyle} padding={'5px 20px'} borderBottom={'1px solid #eee'}>
                {steps.map((step: { title: boolean | React.ReactChild | React.ReactFragment | React.ReactPortal | null | undefined; }, index: React.Key | null | undefined) => (
                    <Step key={index}>
                        <Flex direction="column" gap={"5px"} alignItems={"center"}>
                            <Box flexShrink='0'>
                                <StepTitle>{step.title}</StepTitle>
                            </Box>
                            <StepIndicator>
                                <StepStatus
                                    complete={<StepIcon />}
                                    incomplete={<StepNumber />}
                                    active={<StepNumber />}
                                />
                            </StepIndicator>

                        </Flex>
                        <StepSeparator />
                    </Step>
                ))}
            </Stepper>
            <Box>
                {activeStep === 1 && <>dfsdf</>}
                {activeStep === 2 && <Step1/>}
                {activeStep === 3 && <>AAAA</>}
            </Box>
        </>

    )
}

export default CommonStepper;

const steperStyle = {
    boxShadow: "0px 6px 12px 0px #0000000A",
    backgroundColor: "#fff", // Change background color
    borderRadius: "8px", // Round corners
    padding: "10px", // Add padding
    border: "1px solid #ccc", // Add border
};
