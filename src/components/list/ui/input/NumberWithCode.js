import { Flex } from '@chakra-ui/react'
import React from 'react'
import InputField from './InputField'
import { inputStyle } from '../../../eventbooking/tabsStep/Yourself'
import SelectBox from './SelectBox'
import { countryoptions } from '../../../../utils/constant'

const NumberWithCode = ({ values, errors, handleChange, isSelect = false }) => {
    return (
        <>
            <Flex direction={{ base: "column", md: "row" }} alignItems="baseline" gap={{ base: "5px", md: "15px" }} w="100%" mb={{ base: "20px", md: "40px" }}>
                {isSelect ? <SelectBox options={countryoptions} maxWidth="120px" name="country" style={inputStyle} value={values.country} helperText={errors.country} onChange={handleChange} />
                    : <InputField
                        variant="flushed"
                        style={{ ...inputStyle, fontSize: "14px", maxWidth: "100px", width: "100%" }}
                        type="text"
                        name="code"
                        value={values.code}
                        onChange={handleChange}
                        helperText={errors.code}
                        formControlStyle={{ width: "auto" }}
                    />}
                <InputField   variant="flushed" style={inputStyle} type="number" value={values.number}
                    onChange={handleChange}
                    helperText={errors.number} placeholder="Continue with mobile number" name="number" />
            </Flex>
        </>
    )
}

export default NumberWithCode