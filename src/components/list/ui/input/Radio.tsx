import { Box, FormHelperText, HStack, useRadio, useRadioGroup } from "@chakra-ui/react"
import React from "react"

function RadioCard(props) {
    const { getInputProps, getRadioProps } = useRadio(props)
  
    const input = getInputProps()
    const checkbox = getRadioProps()
  
    return (
      <Box as='label'>
        <input {...input} />
        <Box
          {...checkbox}
          cursor='pointer'
          borderWidth='1px'
          style={RadioStyle}
          color={"#666"}
          bg={"#F5F5F5"}
          _checked={{
            bg: "#28BCA5",
            color: 'white',
            borderColor: "#28BCA5",
          }}
          _focus={{
            boxShadow: 'outline',
          }}
          px={5}
          py={3}
        >
          {props.children}
        </Box>
      </Box>
    )
  }
  

const RadioStyle={
    minWidth:"146px",
    fontSize:"14px",
    fontWeight:500
}


  function ReadioCardComponent({options,onChange,name,value,error}) {
  
    const { getRootProps, getRadioProps } = useRadioGroup({
      name: name,
      defaultValue: value,
      onChange: onChange,
    })
  
    const group = getRootProps()
  
    return (
      <>      <HStack {...group}>
        {options.map((value) => {
          const radio = getRadioProps({ value })
          return (
            <RadioCard key={value} {...radio}>
              {value}
            </RadioCard>
          )
        })}
      </HStack>
      {error && <FormHelperText color="red">{error}</FormHelperText>}
      </>

    )
  }
  
export default ReadioCardComponent