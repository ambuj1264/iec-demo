import React, { Component } from "react";
import Slider from "react-slick";
import { Box, Image } from '@chakra-ui/react'; 

import "slick-carousel/slick/slick.css"; 
import "slick-carousel/slick/slick-theme.css";

export default class BannerSlider extends Component {
  render() {
    const settings = {
        className: "center",
        centerMode: true,
        infinite: true,
        centerPadding: "330px",
        slidesToShow: 1.65,
        responsive: [
          {
            breakpoint: 1440,
            settings: {
              slidesToShow: 1.7,
              slidesToScroll: 1,
              centerPadding: "10px",
            }
          },
          {
            breakpoint: 480,
            settings: {
              slidesToShow: 1,
              slidesToScroll: 1,
              centerPadding: "10px",
            }
          }
        ]
        
    };
    return (
      <Box> 
 
        <Slider {...settings}>
            <Box  padding={'10px'}> 
                    <Image src="/images/banners/banner-1.png" alt="" />   
            </Box>
            <Box   padding={'10px'}>
                <Image src="/images/banners/banner-2.png" alt="" />  
            </Box>
            <Box  padding={'10px'}>
                <Image src="/images/banners/banner-3.png" alt="" />  
            </Box>
        </Slider>
      </Box>
    );
  }
}