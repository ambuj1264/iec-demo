import { Divider } from "@chakra-ui/react";
import * as React from "react";
import styled from "styled-components";

function OrderDetails(props) {
  return (
    <Div>
      <Img
        loading="lazy"
        src="https://cdn.builder.io/api/v1/image/assets/TEMP/e229bd8d0b71ee40eac5fd89f1294359e62e021977f1173b49b65a0e3749c4eb?"
      />
      <Div2>
        <Span>
          <Div3>Yoga & Meditation : Theme Based (35-80 years)</Div3>
          <Span2>
            <Img2
              loading="lazy"
              src="https://cdn.builder.io/api/v1/image/assets/TEMP/288d2799b55b33169237209882ba60bfcb60082c17c5e87acf897e4be56f0453?"
            />
            <Div4>Sun, 24, Apr, 2022 | 04:00 PM</Div4>
          </Span2>
        </Span>
        <Span3>
          <Div5>3</Div5>
          <Div6>Tickets</Div6>
        </Span3>
      </Div2>
      <Span4>
        <Img3
          loading="lazy"
          src="https://cdn.builder.io/api/v1/image/assets/TEMP/c589f5f745895a6f2efc88b39fd9ad1aae1a3e884395900c469a2d0b2f1cb3de?"
        />
        <Div7>Online</Div7>
      </Span4>
      <Span5>
        <Img4
          loading="lazy"
          src="https://cdn.builder.io/api/v1/image/assets/TEMP/25dd8bc97725d5b46c8ab7adbc372020ba1b3376acb18602d70aafdd8c608c2c?"
        />
        <Div8>Zoom</Div8>
      </Span5>
     
      <Divider/>
      <Div9>
        <Span6>
          <Div10>Sub-total</Div10>
          <Span7>
            <Div11>Concenience Fee</Div11>
            <Img6
              loading="lazy"
              src="https://cdn.builder.io/api/v1/image/assets/TEMP/254339caa5da63ce697cb1f51dcd9cdec7fcfdf54dfb92f9e3740236a4e796cf?"
            />
          </Span7>
          <Div12>Total Amount</Div12>
        </Span6>
        <Div13>
          <Span8>
            <Div14>₹</Div14>
            <Div15>450.00</Div15>
          </Span8>
          <Span9>
            <Div16>₹</Div16>
            <Div17>20.00</Div17>
          </Span9>
          <Span10>
            <Div18>₹</Div18>
            <Div19>470.00</Div19>
          </Span10>
        </Div13>
      </Div9>
     <Divider/>
      <Div20 >
       
        <Span11>
          <Img9
            loading="lazy"
            src="https://cdn.builder.io/api/v1/image/assets/TEMP/06a01962296e12e2381c1c58dbf5046ee986158b6de25d2aa19192a288e19fcd?"
          />
          <Div21>Unlock offers or Apply Promocodes</Div21>
        </Span11>
        <Div22>Lorem ipsum dolor sit amet, consectetur</Div22>
      </Div20>
    </Div>
  );
}

const Div = styled.div`
  disply: flex;
  flex-direction: column;
  fill: #fff;
  overflow: hidden;
  position: relative;
  display: flex;
  min-height: 335px;
  max-width: 421px;
  padding: 21px 14px 12px;
`;

const Img = styled.img`
  position: absolute;
  inset: 0;
  height: 100%;
  width: 100%;
  object-fit: cover;
  object-position: center;
`;

const Div2 = styled.div`
  position: relative;
  align-self: stretch;
  display: flex;
  justify-content: space-between;
  gap: 18px;
`;

const Span = styled.span`
  display: flex;
  flex-grow: 1;
  flex-basis: 0%;
  flex-direction: column;
`;

const Div3 = styled.div`
  color: #333;
  text-transform: capitalize;
  white-space: nowrap;
  font: 600 14px/157% Inter, sans-serif;
`;

const Span2 = styled.span`
  display: flex;
  margin-top: 11px;
  justify-content: space-between;
  gap: 8px;
`;

const Img2 = styled.img`
  aspect-ratio: 1;
  object-fit: contain;
  object-position: center;
  width: 18px;
  overflow: hidden;
  max-width: 100%;
`;

const Div4 = styled.div`
  color: #333;
  text-transform: capitalize;
  align-self: start;
  flex-grow: 1;
  flex-basis: auto;
  font: 400 12px/150% Inter, sans-serif;
`;

const Span3 = styled.span`
  align-self: start;
  display: flex;
  flex-basis: 0%;
  flex-direction: column;
`;

const Div5 = styled.div`
  color: #333;
  text-transform: capitalize;
  white-space: nowrap;
  font: 600 20px/90% Inter, sans-serif;
`;

const Div6 = styled.div`
  color: #666;
  text-transform: capitalize;
  margin-top: 9px;
  white-space: nowrap;
  font: 400 12px/167% Inter, sans-serif;
`;

const Span4 = styled.span`
  position: relative;
  align-self: start;
  display: flex;
  margin-top: 14px;
  gap: 10px;
`;

const Img3 = styled.img`
  aspect-ratio: 1;
  object-fit: contain;
  object-position: center;
  width: 14px;
  overflow: hidden;
  max-width: 100%;
`;

const Div7 = styled.div`
  color: #333;
  text-transform: capitalize;
  align-self: center;
  margin: auto 0;
  font: 400 12px/150% Inter, sans-serif;
`;

const Span5 = styled.span`
  position: relative;
  padding-bottom:10px;
  align-self: start;
  display: flex;
  margin-top: 14px;
  width: 57px;
  max-width: 100%;
  gap: 8px;
`;

const Img4 = styled.img`
  aspect-ratio: 1;
  object-fit: contain;
  object-position: center;
  width: 100%;
  overflow: hidden;
  flex: 1;
`;

const Div8 = styled.div`
  color: #333;
  text-transform: capitalize;
  align-self: center;
  margin: auto 0;
  font: 400 12px/150% Inter, sans-serif;
`;

const Img5 = styled.img`
  aspect-ratio: 196.5;
  object-fit: contain;
  object-position: center;
  width: 100%;
  stroke-width: 1px;
  stroke: #eee;
  overflow: hidden;
  align-self: stretch;
  margin-top: 22px;
`;

const Div9 = styled.div`
  position: relative;
  align-self: stretch;
  display: flex;
  margin-top: 20px;
  align-items: start;
  justify-content: space-between;
  gap: 20px;
`;

const Span6 = styled.span`
  display: flex;
  flex-direction: column;
`;

const Div10 = styled.div`
  color: #333;
  font: 400 12px/150% Inter, sans-serif;
`;

const Span7 = styled.span`
  display: flex;
  margin-top: 22px;
  justify-content: space-between;
  gap: 6px;
`;

const Div11 = styled.div`
  color: #333;
  flex-grow: 1;
  white-space: nowrap;
  font: 400 12px/150% Inter, sans-serif;
`;

const Img6 = styled.img`
  aspect-ratio: 1.75;
  object-fit: contain;
  object-position: center;
  width: 7px;
  stroke-width: 1px;
  stroke: #333;
  overflow: hidden;
  align-self: start;
  margin-top: 5px;
  max-width: 100%;
`;

const Div12 = styled.div`
  color: #333;
  margin-top: 22px;
  white-space: nowrap;
  font: 400 13px/138% Inter, sans-serif;
`;

const Div13 = styled.div`
  align-self: stretch;
  display: flex;
  flex-direction: column;
`;

const Span8 = styled.span`
  display: flex;
  align-items: start;
  justify-content: space-between;
  gap: 3px;
`;

const Div14 = styled.div`
  color: #666;
  text-transform: capitalize;
  white-space: nowrap;
  font: 600 8px Inter, sans-serif;
`;

const Div15 = styled.div`
  color: #333;
  text-align: right;
  text-transform: capitalize;
  align-self: stretch;
  flex-grow: 1;
  white-space: nowrap;
  font: 600 13px/154% Inter, sans-serif;
`;

const Span9 = styled.span`
  display: flex;
  margin-top: 16px;
  align-items: start;
  justify-content: space-between;
  gap: 4px;
`;

const Div16 = styled.div`
  color: #666;
  text-transform: capitalize;
  white-space: nowrap;
  font: 600 8px Inter, sans-serif;
`;

const Div17 = styled.div`
  color: #333;
  text-align: right;
  text-transform: capitalize;
  align-self: stretch;
  flex-grow: 1;
  white-space: nowrap;
  font: 600 13px/154% Inter, sans-serif;
`;

const Span10 = styled.span`
  display: flex;
  margin-top: 26px;
  align-items: start;
  justify-content: space-between;
  gap: 2px;
`;

const Div18 = styled.div`
  color: #666;
  text-transform: capitalize;
  white-space: nowrap;
  font: 600 8px Inter, sans-serif;
`;

const Div19 = styled.div`
  color: #333;
  text-align: right;
  text-transform: capitalize;
  align-self: stretch;
  flex-grow: 1;
  white-space: nowrap;
  font: 600 13px/154% Inter, sans-serif;
`;

const Img7 = styled.img`
z-index:-11;
  aspect-ratio: 196.5;
  object-fit: contain;
  object-position: center;
  width: 100%;
  stroke-width: 1px;
  stroke: #eee;
  overflow: hidden;
  align-self: stretch;
  margin-top: 18px;
`;

const Div20 = styled.div`
  background-color: rgb(150 237 203 / 50%) !important;
  disply: flex;
  flex-direction: column;
  position: relative;
  fill: #defffa;
  overflow: hidden;
  align-self: center;
  display: flex;
  aspect-ratio: 7.5576923076923075;
  margin-top: 11px;
  width: 100%;
  align-items: start;
  padding: 12px 80px 12px 12px;
`;

const Span11 = styled.span`
z-index:11;
  position: relative;
  display: flex;
  gap: 6px;
`;

const Img9 = styled.img`
z-index:11;
  aspect-ratio: 1;
  object-fit: contain;
  object-position: center;
  width: 20px;
  overflow: hidden;
  max-width: 100%;
`;

const Div21 = styled.div`
    z-index:11;
  color: black;
  text-transform: capitalize;
  align-self: center;
  flex-grow: 1;
  white-space: nowrap;
  margin: auto 0;
  font: 600 12px Inter, sans-serif;
`;

const Div22 = styled.div`
  position: relative;
  color: #333;
  margin-left: 23px;
  white-space: nowrap;
  font: 400 10px/200% Inter, sans-serif;
`;



export default OrderDetails