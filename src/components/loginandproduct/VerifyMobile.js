import React, { useState } from 'react';
import { useFormik } from 'formik';
import styled from 'styled-components';
import { Button, Stack } from '@chakra-ui/react';

const VerifyMobile = ({ mobile, isEmail }) => {
    const [loginBgColor, setLoginBgColor] = useState("#0A84FF");
    const [loginColor, setLoginColor] = useState("#fff");
    const initialValues = {
        otp1: '',
        otp2: '',
        otp3: '',
        otp4: '',
    };
  
    const onSubmit = (values) => {
        // Handle form submission here
        console.log('Submitted OTP:', values);
    };

    const validate = (values) => {
        const errors = {};
        // Validation logic for OTP fields
        for (let i = 1; i <= 4; i++) {
            const fieldName = `otp${i}`;
            if (!values[fieldName]) {
                errors[fieldName] = 'Required';
            }
        }
        return errors;
    };

    const formik = useFormik({
        initialValues,
        onSubmit,
        validate,
    });

    const handleInputChange = (index, e) => {
        const { value } = e.target;
        const newOtp = { ...formik.values };
        newOtp[`otp${index}`] = value.replace(/\D/g, '').substring(0, 1);
        formik.setValues(newOtp);
    };

    return (
        <Wrapper>
            <Stack spacing={3}>
                <LoginMainHeading fontSize="xl">Verify your Mobile Number</LoginMainHeading>
                <LoginSubHeading fontSize="sm">Enter OTP sent to +91 {mobile}</LoginSubHeading>
            </Stack>
            <form onSubmit={formik.handleSubmit}>
            <OtpWrapper>
                {[1, 2, 3, 4].map((index) => (
                    <OtpInputField
                        key={index}
                        type="text"
                        maxLength="1"
                        name={`otp${index}`}
                        value={formik.values[`otp${index}`]}
                        onChange={(e) => handleInputChange(index, e)}
                    />
                ))}
            </OtpWrapper>
            {(formik.errors.otp1 ||
                formik.errors.otp2 ||
                formik.errors.otp3 ||
                formik.errors.otp4) && <ErrorText>All fields are required</ErrorText>}
            <BottomText>Expect OTP in 21 seconds</BottomText>
            <Button
                                             type="submit"
                                            // bg={loginBgColor}
                                            style={{
                                                width: "100%",
                                                color: loginColor,
                                                bg: "transparent",
                                                border: "none",
                                                borderRadius: "8px",
                                                padding: "20px",
                                                hieght: "220px",
                                                backgroundColor: loginBgColor
                                            }}

                                        >
                                            Submit
                                        </Button>
                                        </form>
        </Wrapper>
    );
};

export default VerifyMobile;

const Wrapper = styled.div``;

const OtpWrapper = styled.div`
  position: relative;
`;
const LoginMainHeading = styled.p`
 font-family: Inter;
 font-size: 16px;
 font-weight: 700;
 line-height: 19px;
 letter-spacing: 0em;
 text-align: left;
`
const LoginSubHeading = styled.p`
 font-family: Inter;
 font-size: 12px;
 font-weight: 500;
 line-height: 16px;
 letter-spacing: 0em;
 text-align: left;
`
const BottomText = styled.div`
font-family: Inter;
font-size: 12px;
font-weight: 600;
line-height: 16px;
letter-spacing: 0em;
text-align: left;
padding-left:5px;
font-family: Inter;
font-size: 12px;
font-weight: 500;
line-height: 16px;
letter-spacing: 0em;
text-align: left;

`;

const OtpInputField = styled.input`
  width: 50px;
  height: 50px;
  text-align: center;
  font-size: 16px;
  margin: 5px;
  border: 1px solid #ccc;
  border-radius: 5px;
  outline: none;
  &:focus {
    border-color: #007bff;
    box-shadow: 0 0 5px rgba(0, 123, 255, 0.5);
  }
`;

const ErrorText = styled.div`
  color: red;
  font-size: 12px;
  margin-top: 5px;
`;
