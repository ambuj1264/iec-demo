import React, { useEffect, useState } from "react";
import TicketDetails from "../eventbooking/tabsStep/TicketDetails";
import styled from "styled-components";
import { Button, Center, Checkbox, Divider } from "@chakra-ui/react";
import { useSelector } from "react-redux";

const Ticket = () => {
  const state = useSelector((state) => state?.ticketDetailsReducer);
  const [loginColor, setLoginColor] = useState("#fff");
  const [loginBgColor, setLoginBgColor] = useState("grey");
  const [submit, setSubmit] = useState(false);
  useEffect(() => {
    const allFieldsRequired = validateObject(state);
    if (allFieldsRequired) {
      setLoginColor("#0A84FF");
      setLoginBgColor("#fff");
      setSubmit(true);
    } else {
      setSubmit(false);
      setLoginColor("#fff");
      setLoginBgColor("grey");
    }
  }, [state]);
  const submitFunc = (e) => {
    e.preventDefault();
    if (submit) {
      console.log("finalData", state);
      alert("submit");
    }
  };
  function validateObject(obj) {
    if (!Object.keys(obj).length >= 1) {
      return false;
    }
    for (const key in obj) {
      if (!obj[key]) {
        return false; // If any field is empty or falsy, return false
      }
    }
    return true; // All fields are filled, return true
  }

  return (
    <Wrapper>
      <TicketDetails />
      <Center height="150px">
        <Divider orientation="vertical" />
      </Center>
      <MainDiv>
        <Span>
          <Div>
            <Checkbox px={4} />
            I accept and have read the <br />
            <span style={{ color: "rgba(10,132,255,1)", paddingLeft: "50px" }}>
              terms and conidtions
            </span>
          </Div>
        </Span>
        <ContinueButton>
          <Button
            type="button"
            onClick={(e) => submitFunc(e)}
            style={{
              backgroundColor: loginColor,
              color: loginBgColor,
              width: "209px",
              height: "36px",
              borderRadius: "8px",
            }}
          >
            Continue
          </Button>
        </ContinueButton>
      </MainDiv>
    </Wrapper>
  );
};

export default Ticket;
const ContinueButton = styled.div`
  display: flex;
  justify-content: center;
  align-items: center;
`;
const MainDiv = styled.div``;
const Wrapper = styled.div`
  display: flex;
  justify-content: space-evenly;
  align-items: center;
`;
const Span = styled.span`
  display: flex;
  align-items: start;
  gap: 8px;
  padding: 0 20px;
`;

const Div = styled.div`
  color: black !important;
  font: 400 14px/20px Inter, sans-serif;
`;
