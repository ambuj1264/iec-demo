import { Center, Divider, Stack, Text, color } from "@chakra-ui/react";
import styled from "@emotion/styled";
import React, { useEffect, useState } from "react";
import {
  VStack,
  Box,
  FormLabel,
  Flex,
  Button,
  Checkbox,
  FormControl,
} from "@chakra-ui/react";
import { useFormik } from "formik";
import { validationForNumber } from "../../utils/validation/ValidationSchema";
import SelectBox from "../list/ui/input/SelectBox";
import { countryoptions } from "../../utils/constant";
import InputField from "../list/ui/input/InputField";
import { GoogleIcon } from "../../utils/SVGIcons/SVG";
import { EmailIcon } from "@chakra-ui/icons";
import VerifyMobile from "./VerifyMobile";

const Login = () => {
  const [loginBgColor, setLoginBgColor] = useState("#EEEEEE");
  const [mobile, setMobile] = useState(0);
  const [loginColor, setLoginColor] = useState("#999999");
  const [phoneSection, setPhoneSection] = useState(true);
  const { values, errors, handleChange, handleSubmit } = useFormik({
    initialValues: { number: "" },
    validationSchema: validationForNumber,

    onSubmit: (values) => {
      console.log(values);
      <VerifyMobile mobile={values?.number} />;
      setPhoneSection(false);
    },
  });
  useEffect(() => {
    if (values?.number?.length == 10) {
      setMobile(values?.number);
      console.log(mobile, "values");
      setLoginBgColor("#0A84FF");
      setLoginColor("#fff");
    } else {
      setLoginBgColor("#EEEEEE");
      setLoginColor("#999999");
    }
  }, [values, errors]);
  console.log(loginBgColor, "loginBgColor");

  return (
    <>
      <Wrapper>
        {phoneSection ? (
          <>
            <PhoneLogin>
              <Stack spacing={3}>
                <LoginMainHeading fontSize="xl">
                  Login with I Elderly Care
                </LoginMainHeading>
                <LoginSubHeading fontSize="sm">
                  This won’t take long!
                </LoginSubHeading>
                <VStack align="stretch" mx="auto" my={"10px"}>
                  <form onSubmit={handleSubmit}>
                    <Box mb={"25px"}>
                      <Flex gap={"20px"}>
                        <SelectBox
                          variant="flushed"
                          options={countryoptions}
                          maxWidth="100px"
                          name="contry"
                          onChange={handleChange}
                          value={values.contry}
                          helperText={errors.contry}
                       
                        />
                        <InputField
                          variant="flushed"
                          labelStyle={labelStyle}
                          placeholder="Please enter your number"
                          name="number"
                          type="text"
                          value={values.number}
                          helperText={errors.number}
                          onChange={handleChange}
                        />
                      </Flex>
                    </Box>

                    <Button
                      type="submit"
                      // bg={loginBgColor}
                      style={{
                        width: "100%",
                        color: loginColor,
                        bg: "transparent",
                        border: "none",
                        borderRadius: "8px",
                        padding: "20px",
                        hieght: "220px",
                        backgroundColor: loginBgColor,
                      }}
                    >
                      Submit
                    </Button>
                  </form>
                </VStack>
              </Stack>
            </PhoneLogin>{" "}
          </>
        ) : (
          <>
            {" "}
            <VerifyMobile mobile={mobile} />
          </>
        )}

        <Center height="150px">
          <Divider orientation="vertical" />
        </Center>
        <SocialLogin>
          <Stack spacing={3}>
            <Box width="100%" my={{ base: "5px", md: "10px" }}>
              <StyledButton px={{ base: "15px", md: "25px" }} py="25px">
                <GoogleIcon />
                <Text width="100%">Continue with Google</Text>
              </StyledButton>
              <StyledButton px={{ base: "15px", md: "25px" }} py="25px">
                <EmailIcon />
                <Text width="100%">Continue with Email</Text>
              </StyledButton>
            </Box>
          </Stack>
        </SocialLogin>
      </Wrapper>
    </>
  );
};

export default Login;

const Wrapper = styled.div`
  display: flex;
  justify-content: space-evenly;
  align-items: center;
`;
const PhoneLogin = styled.div``;
const SocialLogin = styled.div``;
const LoginMainHeading = styled.p`
  font-family: Inter;
  font-size: 16px;
  font-weight: 700;
  line-height: 19px;
  letter-spacing: 0em;
  text-align: left;
`;
const LoginSubHeading = styled.p`
  font-family: Inter;
  font-size: 12px;
  font-weight: 500;
  line-height: 16px;
  letter-spacing: 0em;
  text-align: left;
`;

export const labelStyle = {
  color: "#666",
  fontFamily: "Inter",
  fontSize: "12px",
  fontWeight: 500,
  lineHeight: "19px",
};
export const buttonStyle = {
  width: "100%",
  color: "#999999",
  bg: "transparent",
  border: "none",
  borderRadius: "8px",
  padding: "20px",
  hieght: "220px",
};
const StyledButton = styled(Button)`
  display: flex !important;
  justify-content: start !important;
  align-items: center !important;
  color: #333333 !important;
  font-size: 14px !important;
  font-weight: 500 !important;
  border-radius: 0 !important;
  border: 1px solid #eee !important;
  width: 100% !important;
  padding: ${({ theme }) => theme.px};
  margin-bottom: 15px !important;

  @media (min-width: 48em) {
    padding: ${({ theme }) => theme.pxMd};
  }
`;
export const inputStyle = {
  // border: "none !important",
  // borderRadius: "8px", // Adding border radius
  // borderBottom: "1px solid #eee",
  // borderColor: "gray.400",
};
