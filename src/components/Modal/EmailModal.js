import React from 'react';
import { Formik, Form, Field, ErrorMessage } from 'formik';
import {
  Modal,
  ModalOverlay,
  ModalContent,
  ModalHeader,
  ModalCloseButton,
  ModalBody,
  FormControl,
  FormLabel,
  ModalFooter,
  Input,
  ModalLeftButton,
} from '@chakra-ui/react';
import * as Yup from 'yup';
import { ArrowBackIcon } from '@chakra-ui/icons';
import ModalButton from '../common/Button/ModalButton';

const EmailModal = ({ isOpen, onClose }) => {
  const initialRef = React.useRef(null);
  const finalRef = React.useRef(null);

  const initialValues = {
    email: '',
  };

  const validationSchema = Yup.object({
    email: Yup.string().email('Invalid email address').required('Email is required'),
  });

  const handleSubmit = (values, { setSubmitting }) => {
    // Handle form submission logic here
    console.log(values);
    onClose(); // Close modal after submission (you may adjust this according to your logic)
    setSubmitting(false);
  };

  return (
    <Modal
      initialFocusRef={initialRef}
      finalFocusRef={finalRef}
      isOpen={isOpen}
      onClose={onClose}
    >
      <ModalOverlay />
      <ModalContent
        width="376px"
        height="500px"
        style={{
          maxWidth: 'unset', // To override Chakra UI's default max-width for the modal content
        }}
      >
        <ModalHeader>
          <ArrowBackIcon style={{ cursor: 'pointer' }} />
        </ModalHeader>
        <ModalCloseButton mt={2} />
        <Formik
          initialValues={initialValues}
          validationSchema={validationSchema}
          onSubmit={handleSubmit}
        >
          {({ isSubmitting }) => (
            <Form>
              <ModalBody pb={6}>
                <FormControl>

                  <FormLabel>Email</FormLabel>
                  <Field name="email">
                    {({ field }) => (
                        <Input
                      {...field}
                      ref={initialRef}
                      placeholder="Email"
                      variant="flushed" 
                    />
                    )}
                  </Field>
                  <ErrorMessage name="email" />
                </FormControl>
              </ModalBody>

              <ModalFooter>
                <ModalButton
                  type="submit"
                  colorScheme="blue"
               
                  isLoading={isSubmitting}
                >
                  Save
                </ModalButton>
              </ModalFooter>
            </Form>
          )}
        </Formik>
      </ModalContent>
    </Modal>
  );
};

export default EmailModal;
