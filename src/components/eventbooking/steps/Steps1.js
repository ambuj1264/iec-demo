import { Box, Text } from "@chakra-ui/react";
import TabsComponent from "../../list/ui/tab/Tabs";
import { tabsData } from "../../../utils/constant";

const Step1 = () => {
    return <Box p="25px">
        <Box>
            <Text style={headingTextStyle}>Booking For Loved One</Text>
            <Text as={"p"} style={headingParaText}>We make sure that your Loved ones are informed and assistant throughout their Journey</Text>
        </Box>
        <Box style={{ border: "1px solid #EEEEEE" }} p="25px">
            <TabsComponent tabsData={tabsData} />
        </Box>
    </Box>
}

export default Step1;

const headingTextStyle = {
    color: '#333',
    fontFamily: 'Inter',
    fontSize: '20px',
    fontStyle: 'normal',
    fontWeight: 600,
    lineHeight: 'normal',
    textTransform: 'capitalize',
}

const headingParaText = {
    fontSize: "16px",
    fontWeight: 500,
    lineHeight: "14px",
    margin: "15px 0",
    color: "#666"
}
