import { useRouter } from "next/router";
import { useFormik } from "formik";
import {
  VStack,
  Box,
  FormLabel,
  Flex,
  Button,
  Checkbox,
  FormControl,
} from "@chakra-ui/react";
import InputField from "../../list/ui/input/InputField";
import SelectBox from "../../list/ui/input/SelectBox";
import { validationSchema } from "../../../utils/validation/ValidationSchema";
import ReadioCardComponent from "../../list/ui/input/Radio";
import { countryoptions, tickettypeoptions } from "../../../utils/constant";

const Yourself = () => {
  const { values, errors, handleChange, handleSubmit } = useFormik({
    initialValues: {
      name: "",
      email: "",
      contry: "",
      number: "",
      tickettype: "Bronze",
    },
    validationSchema: validationSchema,
    onSubmit: (values) => {
      console.log(values);
      router.push("/booking/bookingprecheckout");
    },
  });

  return (
    <>
      <VStack align="stretch" mx="auto" my={"10px"}>
        <form onSubmit={handleSubmit}>
          <Box mb={"15px"}>
            <InputField
              label="Full Name*"
              variant="flushed"
              labelStyle={labelStyle}
              placeholder="Please enter your name"
              name="name"
              type="text"
              value={values.name}
              helperText={errors.name}
              onChange={handleChange}
            />
          </Box>
          <Box mb={"25px"}>
            <FormLabel style={labelStyle}>Mobile Number*</FormLabel>
            <Flex gap={"20px"}>
              <SelectBox
                variant="flushed"
                options={countryoptions}
                maxWidth="160px"
                name="contry"
                onChange={handleChange}
                value={values.contry}
                helperText={errors.contry}
              />
              <InputField
                labelStyle={labelStyle}
                variant="flushed"
                placeholder="Please enter your number"
                name="number"
                type="number"
                value={values.number}
                helperText={errors.number}
                onChange={handleChange}
              />
            </Flex>
          </Box>
          <Box mb={"25px"}>
            <InputField
              label="Email*"
              labelStyle={labelStyle}
              type="email"
              variant="flushed"
              placeholder="Please enter your email"
              name="email"
              value={values.email}
              helperText={errors.email}
              onChange={handleChange}
            />
          </Box>
          <Flex
            mb={"30px"}
            style={{ display: "flex", alignItems: "center", gap: "20px" }}
          >
            <FormLabel style={{ ...labelStyle, margin: "0px" }}>
              Select Ticket Type
            </FormLabel>
            <ReadioCardComponent
              options={tickettypeoptions}
              onChange={handleChange}
              name="tickettype"
              value={values.tickettype}
              error={errors.tickettype}
            />
          </Flex>
          <Button
            type="submit"
            style={buttonStyle}
            _hover={{
              bg: "transparent",
              borderColor: "#28BCA5",
              color: "#28BCA5",
            }}
          >
            Submit
          </Button>
          <FormControl mt={"25px"}>
            <Checkbox style={checkboxstyle} defaultChecked>
              Save & Secure details for Future Use.
            </Checkbox>
          </FormControl>
        </form>
      </VStack>
    </>
  );
};

export default Yourself;



const labelStyle = {
  color: "#666",
  fontFamily: "Inter",
  fontSize: "12px",
  fontWeight: 500,
  lineHeight: "19px" /* 158.333% */,
};

const buttonStyle = {
  width: "100%",
  color: "#28BCA5",
  bg: "transparent",
  borderColor: "#28BCA5",
  borderRadius: "0px",
  padding: "25px",
  maxHeight: "40px",
};

const checkboxstyle = {
  color: "#666",
  fontFamily: "Inter",
  fontSize: "14px",
  fontWeight: 400,
  textTransform: "capitalize",
};
