import React, {useState} from 'react';
import {
    Stepper,
    Step,
    StepStatus,
    StepIndicator,
    StepIcon,
    StepNumber,
    StepTitle,
    StepDescription,
    StepSeparator,
    Box,
    useSteps,
    Button,
    Text,
    ButtonGroup
} from '@chakra-ui/react'
import { AddIcon, MinusIcon } from '@chakra-ui/icons';
import * as moment from 'moment';
import { getUniqueDates } from '../../../libs/utils';
 
const DateTimeBlock = ({data,setActiveStep, showDates}) => {  
    const [activeDate, setActiveDate] = useState();
    const [activTime, setActiveTime] = useState();
    // const [showDates, setUniqueDates] = useState(getUniqueDates(data))
    // const dateString = moment(event?.startDateTime).format('ddd DD, MMM')
    // const timeString = moment(event?.startDateTime).format('HH:mm A');
    console.log('showDates', showDates);
     
    return <Box>
               
                <Box padding={'20px'} display={'flex'} flexWrap={'wrap'} gap={10}>
                   
                    {showDates && showDates.map((dateString, index) => { 
                        const bgColor = activeDate === dateString ? '#28BCA5': '#F5F5F5';
                        return <Button background={bgColor} border={'none'} width={'300px'} onClick={() =>setActiveDate(dateString)} borderRadius={0} paddingBlock={5} textAlign={'left'} color={'#333'}>{dateString}</Button>
                    })}
                     
                </Box>
                <Box  padding={'20px'} justifyContent={'center'} display={'flex'} flexDir={'column'}  borderTop={'1px solid #eee'}>
                    {activeDate && <Text pb={10}>{activeDate}</Text> }
                    <Box display={'flex'} flexWrap={'wrap'} gap={10}>
                        {activeDate && data.filter(tickets =>  moment(tickets?.startDate).format('dddd DD MMM, YYYY') === activeDate).map((time, index) => {
                           console.log(activeDate && data.filter(tickets => tickets.startDate === activeDate))
                           const timeString = moment(time?.startDate).format('HH:mm A');
                           console.log('time string', time?.startDate, moment(time?.startDate).format('HH:mm A'))
                            const bgColor = activTime === timeString ? '#28BCA5': '#F5F5F5';
                            return <Button background={bgColor} border={'none'} onClick={() =>setActiveTime(timeString)}  borderRadius={0} paddingBlock={5} textAlign={'left'} color={'#333'}>{timeString}</Button>
                            })

                        }
                    </Box>
                </Box>
                <Box  padding={'20px'} justifyContent={'center'} display={'flex'}  borderTop={'1px solid #eee'}>
                    <Button onClick={() => setActiveStep(1)} width={'7xl'} variant={'solid'} borderRadius={0} paddingBlock={5}>Continue</Button>
                </Box>
       
        
    </Box>
}


const PlusMinusQuantity = ({ value, minValue = 0, maxValue = Infinity, onChange }) => {
    const handleIncrement = () => {
      if (value < maxValue) {
        onChange(value + 1);
      }
    };
  
    const handleDecrement = () => {
      if (value > minValue) {
        onChange(value - 1);
      }
    };
  
    return (
      <ButtonGroup>
        <Button leftIcon={<MinusIcon />} onClick={handleDecrement} isDisabled={value <= minValue}>
          
        </Button>
            <Box padding={1}>{value}</Box>
        <Button rightIcon={<AddIcon />} onClick={handleIncrement} isDisabled={value >= maxValue}>
          
        </Button>
      </ButtonGroup>
    );
  }


const TicketsBlock = ({setActiveStep}) => {
    const [quantity, setQuantity] = useState(1);
    const handleQuantityChange = (newQuantity) => {
        setQuantity(newQuantity);
      };
    return <Box>
        <Box padding={'20px'} display={'flex'} flexWrap={'wrap'} gap={10}>
            <Box display={'flex'} flex={1} flexDir={'row'} paddingBottom={4} borderBottom={'1px solid #eee'}>
                <Text flex={2}>Your Self</Text>
                <PlusMinusQuantity value={quantity}
                    minValue={1}
                    maxValue={10}
                    onChange={handleQuantityChange}/>
            </Box>
        </Box>
        <Box  padding={'20px'} justifyContent={'center'} display={'flex'} flexDir={'column'}  borderTop={'1px solid #eee'}>
            <Text pb={10}>Monday,08 April</Text>
            <Box display={'flex'} flexWrap={'wrap'} gap={10}>
                <Button background={'#F5F5F5'} border={'none'}  borderRadius={0} paddingBlock={5} textAlign={'left'} color={'#333'}>1:00 PM</Button>
                <Button background={'#F5F5F5'} border={'none'} borderRadius={0} paddingBlock={5} textAlign={'left'} color={'#333'}>2:00 PM</Button>
                <Button background={'#F5F5F5'} border={'none'} borderRadius={0} paddingBlock={5} textAlign={'left'} color={'#333'}>3:00 PM</Button>
                <Button background={'#F5F5F5'} border={'none'} borderRadius={0} paddingBlock={5} textAlign={'left'} color={'#333'}>4:00 PM</Button>
            </Box>
        </Box>
        <Box  padding={'20px'} justifyContent={'center'} display={'flex'}  borderTop={'1px solid #eee'}>
            <Button onClick={() => setActiveStep(1)} width={'7xl'} variant={'solid'} borderRadius={0} paddingBlock={5}>Continue</Button>
        </Box>


    </Box>
}

const RegisterBlock = ({setActiveStep}) => {
    return <Box background={'##F5F5F5'}>
        RegisterBlock
        <Button onClick={() => setActiveStep(1)}>Back</Button>
        {/* <Button onClick={() => setActiveStep(2)}>Next</Button> */}
    </Box>
}

const steps = [
    { title: 'Date & Time', description: 'Contact Info', component: <DateTimeBlock /> },
    { title: 'Quantity & Tickets', description: 'Date & Time' },
    { title: 'Register', description: 'Select Rooms' },
  ]
  



const EventBookingSteps = ({ticketTypesDetails}) => {
    const { activeStep, setActiveStep } = useSteps({
      index: 0,
      count: steps.length,
    })
    console.log('activeStep', activeStep)
    
    const uniqeShowdates = getUniqueDates(ticketTypesDetails || [])
    return (<>
        <Stepper index={activeStep}  padding={'20px'} borderBottom={'1px solid #eee'}>
            {steps.map((step, index) => (
            <Step key={index}>
                <StepIndicator>
                <StepStatus
                    complete={<StepIcon />}
                    incomplete={<StepNumber />}
                    active={<StepNumber />}
                />
                </StepIndicator>
    
                <Box flexShrink='0'>
                <StepTitle>{step.title}</StepTitle>
                {/* <StepDescription>{step.description}</StepDescription> */}
                </Box>
    
                <StepSeparator />
            </Step>
            ))}
        </Stepper>
        <Box>
            {activeStep === 0 && <DateTimeBlock data={ticketTypesDetails} showDates={uniqeShowdates} setActiveStep={setActiveStep}/>}
            {activeStep === 1 && <TicketsBlock setActiveStep={setActiveStep}/>}
            {activeStep === 2 && <RegisterBlock setActiveStep={setActiveStep}/>}
        </Box>
    </>
      
    )
  }
  export default EventBookingSteps;