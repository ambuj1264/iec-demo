import React, { useState} from 'react';
import { 
    Box,
    Stack,
    Heading,
    Icon,
    Text
} from '@chakra-ui/react';
import { BiLeftArrow, BiSolidLeftArrow } from 'react-icons/bi';
import { CloseIcon } from '@chakra-ui/icons';
import { useRouter } from 'next/router';

const Header = ({title, startTime, venue,BackIcon}) => {
    const router = useRouter();

    const handleCross=()=>{
        router.push("/events")
    }
    return <Box display={'flex'} flexDir={'row'} background={'#E2F8FF'}  padding={'20px'} mb={5}>
       <Box>
            {BackIcon ? <BackIcon/> : <Icon as={BiLeftArrow} />}
       </Box>
       <Box flex={2} ml={10}>
           <Heading as={'h2'} lineHeight={'28px'} mb={'5px'} fontSize={'20px'} color={'#333333'}>{title}</Heading>
           <Text fontSize={'14px'} lineHeight={'18px'} color={'#666666'}>at {startTime} {venue?.venueName}: {venue?.street1}</Text>
       </Box>
       <Box>
            <Icon as={CloseIcon} onClick={handleCross}  cursor="pointer"/>
       </Box>
    </Box>
}

export default Header;