import React from 'react';
import Layout from '../components/layout';
import {
    VStack,
    Stack,
    Box,
    Text,
    SimpleGrid,
    GridItem,
    Image,
    Heading,
    Link
} from '@chakra-ui/react'


const HomePage = () => {
    return <VStack>
        <Layout showBanner={true}>
           <Stack maxW={'7xl'}>
                <Text textAlign={'center'} marginBottom={'40px'} fontSize={'32px'} fontWeight={'600px'}>Find What the need</Text>
                <SimpleGrid columns={2}  columnGap={'20px'}>
                    <GridItem>
                        <Image src={'/images/home/on-ground-event.png'} />
                    </GridItem>
                    <GridItem>
                        <Heading fontSize={'34px'} lineHeight={'72px'}>Events</Heading>
                        <Text fontSize={'14px'} fontWeight={'600'} lineHeight={'42px'}>Some Basic Points how the event works</Text>
                        <Text fontSize={'12px'} fontWeight={'400'} lineHeight={'28px'}>Building mobile applications is never easy.  P2P lending app ? Logistic App ? E-Commerce App ? 
                            We have done them all. Done them all well. Done them all fast. Building mobile applications is never easy.  P2P lending app ? Logistic App ? E-Commerce App ? 
                            We have done them all. Done them all well. Done them all fast.      
                        </Text>
                        <SimpleGrid columns={'3'} marginBlock={'50px'}>
                            <GridItem textAlign={'center'}>
                                <Text fontSize={'48px'} fontWeight={'600'} color={'#05B09C'}>80+</Text>
                                <Text fontSize={'20px'} fontWeight={'600'}>Successful Events</Text>
                            </GridItem>
                            <GridItem textAlign={'center'}>
                                <Text fontSize={'48px'} fontWeight={'600'} color={'#05B09C'}>2,332+</Text>
                                <Text fontSize={'20px'} fontWeight={'600'}>Happy Audience</Text>
                            </GridItem>
                            <GridItem textAlign={'center'}>
                                <Text fontSize={'48px'} fontWeight={'600'} color={'#05B09C'}>24/7</Text>
                                <Text fontSize={'20px'} fontWeight={'600'}>Support</Text>
                            </GridItem>
                            
                        </SimpleGrid>
                        <Link display={'block'} width={'190px'} textAlign={'center'} fontSize={'18px'} marginBlock={'30px'} background={'#0984ff'} padding={'10px 20px'} color={'#fff'}> BOOK NOW </Link>
                    </GridItem>
                </SimpleGrid>
                <SimpleGrid columns={2} gridTemplateColumns={'2fr 1fr'} columnGap={'20px'} mt={'50px'}>
                    <GridItem maxW={'700px'}>
                        <Heading fontSize={'34px'} lineHeight={'72px'}>Nursing</Heading>
                        <Text fontSize={'14px'} fontWeight={'600'} lineHeight={'42px'}>Some Basic Points how the Nursing work</Text>
                        <Text fontSize={'12px'} fontWeight={'400'} lineHeight={'28px'}>Building mobile applications is never easy.  P2P lending app ? Logistic App ? E-Commerce App ? 
                        We have done them all. Done them all well. Done them all fast. Building mobile applications is never easy.  P2P lending app ? Logistic App ? E-Commerce App ? 
                        We have done them all. Done them all well. Done them all fast.       
                        </Text>
                        <SimpleGrid columns={'3'} marginBlock={'50px'}>
                            <GridItem textAlign={'center'}>
                                <Text fontSize={'48px'} fontWeight={'600'} color={'#1A73E7'}>1,726</Text>
                                <Text fontSize={'20px'} fontWeight={'600'}>Vendors</Text>
                            </GridItem>
                            <GridItem textAlign={'center'}>
                                <Text fontSize={'48px'} fontWeight={'600'} color={'#1A73E7'}>2,332+</Text>
                                <Text fontSize={'20px'} fontWeight={'600'}>Users</Text>
                            </GridItem>
                            <GridItem textAlign={'center'}>
                                <Text fontSize={'48px'} fontWeight={'600'} color={'#1A73E7'}>24/7</Text>
                                <Text fontSize={'20px'} fontWeight={'600'}>Support</Text>
                            </GridItem>
                            
                        </SimpleGrid>
                        <Link display={'block'} width={'190px'} textAlign={'center'} fontSize={'18px'} marginBlock={'30px'} background={'#0984ff'} padding={'10px 20px'} color={'#fff'}> BOOK NOW </Link>
                    </GridItem>
                    <GridItem display={'flex'} justifyContent={'center'}>
                        <Image src={'/images/home/nursing.png'} />
                    </GridItem>
                </SimpleGrid>
                 
           </Stack>
        </Layout>
    </VStack>
}

export default HomePage;