import { useRouter } from "next/router";
import * as React from "react";
import styled from "styled-components";
import Header from "../../components/layout/header";
import { useState } from "react";
import Login from "../../components/login/Login"
import Layout from "../../components/layout";
import ModalComponent from "../../components/list/ui/modals/ModalComponent"
function BookingCheckout(props) {
    const router = useRouter()
  const navigate=(destination)=>{
    router.push(destination);
  }
  const [open, setOpen] = useState(false);
  const onHandleClose = () => {
      setOpen(false)
  }


  return (
      <Layout >
    <Div >
      <Div2>
        <RightCursor
          loading="lazy"
          src="https://cdn.builder.io/api/v1/image/assets/TEMP/619086c29b0f0859d7d1fd1381ab234a8cde12daf2ada6ac1b97f6cb88a975a5?apiKey=9aef8ecf740c47caa7855ca5ffbd8b31&"
          onClick={()=>navigate("/booking")}
        />
        <BannerLogo
          loading="lazy"
          srcSet="https://cdn.builder.io/api/v1/image/assets/TEMP/7e42b6080c588a4b751a4ea993448c7d20be0d615b5cbfd2654bf70a227dcd95?apiKey=9aef8ecf740c47caa7855ca5ffbd8b31&width=100 100w, https://cdn.builder.io/api/v1/image/assets/TEMP/7e42b6080c588a4b751a4ea993448c7d20be0d615b5cbfd2654bf70a227dcd95?apiKey=9aef8ecf740c47caa7855ca5ffbd8b31&width=200 200w, https://cdn.builder.io/api/v1/image/assets/TEMP/7e42b6080c588a4b751a4ea993448c7d20be0d615b5cbfd2654bf70a227dcd95?apiKey=9aef8ecf740c47caa7855ca5ffbd8b31&width=400 400w, https://cdn.builder.io/api/v1/image/assets/TEMP/7e42b6080c588a4b751a4ea993448c7d20be0d615b5cbfd2654bf70a227dcd95?apiKey=9aef8ecf740c47caa7855ca5ffbd8b31&width=800 800w, https://cdn.builder.io/api/v1/image/assets/TEMP/7e42b6080c588a4b751a4ea993448c7d20be0d615b5cbfd2654bf70a227dcd95?apiKey=9aef8ecf740c47caa7855ca5ffbd8b31&width=1200 1200w, https://cdn.builder.io/api/v1/image/assets/TEMP/7e42b6080c588a4b751a4ea993448c7d20be0d615b5cbfd2654bf70a227dcd95?apiKey=9aef8ecf740c47caa7855ca5ffbd8b31&width=1600 1600w, https://cdn.builder.io/api/v1/image/assets/TEMP/7e42b6080c588a4b751a4ea993448c7d20be0d615b5cbfd2654bf70a227dcd95?apiKey=9aef8ecf740c47caa7855ca5ffbd8b31&width=2000 2000w, https://cdn.builder.io/api/v1/image/assets/TEMP/7e42b6080c588a4b751a4ea993448c7d20be0d615b5cbfd2654bf70a227dcd95?apiKey=9aef8ecf740c47caa7855ca5ffbd8b31&"
        />
        <Cross
          loading="lazy"
          src="https://cdn.builder.io/api/v1/image/assets/TEMP/7535eeb8832468b511ec1c4b0e1cf5faa0df04407b89e9b62d81fddeb24e78f4?apiKey=9aef8ecf740c47caa7855ca5ffbd8b31&"
          onClick={()=>navigate("/events")}
        />
      </Div2>
      <Div3>
        <Img4
          loading="lazy"
          src="https://cdn.builder.io/api/v1/image/assets/TEMP/c4e560d00c876db4223f209bd942126c258c4709849286558d57a7d94e0e5ca9?apiKey=9aef8ecf740c47caa7855ca5ffbd8b31&"
        />
        <Div4>
          <Img5
            loading="lazy"
            src="https://cdn.builder.io/api/v1/image/assets/TEMP/18e954de4b74287e9847bf51b498a079ab9e1d7ebcd192ae0e1d4a904f707034?apiKey=9aef8ecf740c47caa7855ca5ffbd8b31&"
          />
          <Div5>
            <Div6>Online Event</Div6>
            <Div7>Login & save the planet. Use your phone as a ticket</Div7>
          </Div5>
          <Img6
            loading="lazy"
            src="https://cdn.builder.io/api/v1/image/assets/TEMP/44964a984ec6e26ccce430966e262a4b18e146ac0074ba5876679fb19209866b?apiKey=9aef8ecf740c47caa7855ca5ffbd8b31&"
          />
        </Div4>
        <Div8>
          <Div9>Yoga & Meditations : Theme Based senior (35-80 years)</Div9>
          <Div10>
            <Div11>₹</Div11>
            <Div12>450</Div12>
          </Div10>
        </Div8>
        <Div13>2 Tickets</Div13>
        <Img7
          loading="lazy"
          src="https://cdn.builder.io/api/v1/image/assets/TEMP/33f619f9d8365e50b736b455d65b97487452fe2c1bda6a59b246b376012af156?apiKey=9aef8ecf740c47caa7855ca5ffbd8b31&"
        />
        <Div14>
          <Img8
            loading="lazy"
            src="https://cdn.builder.io/api/v1/image/assets/TEMP/dd6fb0a4eaaf25c6b56e9e0600305d163c326b88224d4dbef38e2aa55850b8de?apiKey=9aef8ecf740c47caa7855ca5ffbd8b31&"
          />
          <Div15>Sun 16, Oct | 6:30PM</Div15>
        </Div14>
        <Div16>
          <Img9
            loading="lazy"
            src="https://cdn.builder.io/api/v1/image/assets/TEMP/2c144956783af88a3edad8a1ff949891da8f837d408758379df6c647fe7bbbcf?apiKey=9aef8ecf740c47caa7855ca5ffbd8b31&"
          />
          <Div17>Online</Div17>
        </Div16>
        <Div18>
          <Img10
            loading="lazy"
            src="https://cdn.builder.io/api/v1/image/assets/TEMP/2581d05a74ccca45b76eb0759e382a141a931bff3450cd823cd616ca927d731c?apiKey=9aef8ecf740c47caa7855ca5ffbd8b31&"
          />
          <Div19>Zoom India</Div19>
        </Div18>
        <Img11
          loading="lazy"
          src="https://cdn.builder.io/api/v1/image/assets/TEMP/33f619f9d8365e50b736b455d65b97487452fe2c1bda6a59b246b376012af156?apiKey=9aef8ecf740c47caa7855ca5ffbd8b31&"
        />
        <Div20>
          <Div21>
            <Img12
              loading="lazy"
              src="https://cdn.builder.io/api/v1/image/assets/TEMP/f2bdadec2a0c94ad312b509db804b595b88c175835ea863e3bfff3b839a947a7?apiKey=9aef8ecf740c47caa7855ca5ffbd8b31&"
            />
            <Div22>Gold | Silver | Bronze (Tickets)</Div22>
          </Div21>
          <Div23>3</Div23>
        </Div20>
      </Div3>
      <Div24>
        <Div25>Sub-total</Div25>
        <Div26>
          <Div27>₹</Div27>
          <Div28>450.00</Div28>
        </Div26>
      </Div24>
      <Div29>
        <Div30>Booking Fee</Div30>
        <Div31>
          <Div32>₹</Div32>
          <Div33>20.00</Div33>
        </Div31>
      </Div29>
      <Img13
        loading="lazy"
        src="https://cdn.builder.io/api/v1/image/assets/TEMP/477bfa59fcd84cd283865ea61518fc761b56a12b55df4d8100f1dab6c3a3f365?apiKey=9aef8ecf740c47caa7855ca5ffbd8b31&"
      />
      <Div34>
        <Div35>Total Amount</Div35>
        <Div36>
          <Div37>₹</Div37>
          <Div38>470.00</Div38>
        </Div36>
      </Div34>
      <Div39>
        <LongButton   onClick={setOpen}>
          <ButtonImage
            loading="lazy"
            src="https://cdn.builder.io/api/v1/image/assets/TEMP/c6c7256ad66751c23c4c38e42efe3ea08fbf40e221c47880bf1df5f7a3fba841?apiKey=9aef8ecf740c47caa7855ca5ffbd8b31&"
          />
          LOGIN TO PROCEED
        </LongButton>
        <ModalComponent isCancelIcon={false} isOpen={open} onClose={onHandleClose}>
                <Login />
            </ModalComponent>
      </Div39>
    </Div>
   
    </Layout>

  );
}

const Div = styled.div`
  margin-top:-40px;
  display: flex;
  flex-direction: column;
  background-color:#fff;
`;

const Div2 = styled.div`
  align-self: center;
  z-index: 1;
  display: flex;
  width: 744px;
  max-width: 100%;
  justify-content: space-between;
  gap: 20px;
  padding: 0 20px;
  @media (max-width: 991px) {
    flex-wrap: wrap;
  }
`;

const RightCursor = styled.img`
  cursor:pointer;
  aspect-ratio: 1.17;
  object-fit: contain;
  object-position: center;
  width: 21px;
  fill: #333;
  overflow: hidden;
  align-self: center;
  max-width: 100%;
  margin: auto 0;
`;

const BannerLogo = styled.img`
  aspect-ratio: 1.42;
  object-fit: contain;
  object-position: center;
  width: 176px;
  overflow: hidden;
  max-width: 100%;
`;

const Cross = styled.img`
  aspect-ratio: 1;
  object-fit: contain;
  object-position: center;
  width: 24px;
  overflow: hidden;
  align-self: center;
  max-width: 100%;
  margin: auto 0;
  cursor:pointer;
`;

const Div3 = styled.div`
  disply: flex;
  flex-direction: column;
  fill: #fff;
  stroke-width: 1px;
  stroke: #eee;
  filter: drop-shadow(0px 8px 24px rgba(0, 0, 0, 0.06));
  overflow: hidden;
  align-self: center;
  position: relative;
  display: flex;
  min-height: 357px;
  width: 776px;
  max-width: 100%;
  padding: 22px 20px;
`;

const Img4 = styled.img`
  position: absolute;
  inset: 0;
  height: 100%;
  width: 100%;
  object-fit: cover;
  object-position: center;
`;

const Div4 = styled.div`
  disply: flex;
  flex-direction: column;
  position: relative;
  fill: #e2f8ff;
  overflow: hidden;
  align-self: center;
  display: flex;
  min-height: 60px;
  width: 100%;
  justify-content: space-between;
  gap: 20px;
  padding: 12px 16px;
  @media (max-width: 991px) {
    max-width: 100%;
    flex-wrap: wrap;
  }
`;

const Img5 = styled.img`
  position: absolute;
  inset: 0;
  height: 100%;
  width: 100%;
  object-fit: cover;
  object-position: center;
`;

const Div5 = styled.div`
  position: relative;
  display: flex;
  flex-direction: column;
`;

const Div6 = styled.div`
  color: #333;
  text-transform: capitalize;
  font: 600 18px Inter, sans-serif;
`;

const Div7 = styled.div`
  color: #333;
  margin-top: 10px;
  white-space: nowrap;
  font: 400 14px/143% Inter, sans-serif;
  @media (max-width: 991px) {
    white-space: initial;
  }
`;

const Img6 = styled.img`
  aspect-ratio: 1;
  object-fit: contain;
  object-position: center;
  width: 20px;
  overflow: hidden;
  align-self: center;
  max-width: 100%;
  margin: auto 0;
`;

const Div8 = styled.div`
  position: relative;
  align-self: stretch;
  display: flex;
  margin-top: 22px;
  width: 100%;
  align-items: start;
  justify-content: space-between;
  gap: 20px;
  @media (max-width: 991px) {
    max-width: 100%;
    margin-right: 7px;
    flex-wrap: wrap;
  }
`;

const Div9 = styled.div`
  color: #333;
  text-transform: capitalize;
  flex-grow: 1;
  flex-basis: auto;
  font: 600 20px/140% Inter, sans-serif;
  @media (max-width: 991px) {
    max-width: 100%;
  }
`;

const Div10 = styled.div`
  display: flex;
  align-items: start;
  gap: 4px;
`;

const Div11 = styled.div`
  color: #333;
  text-transform: capitalize;
  white-space: nowrap;
  font: 600 12px Inter, sans-serif;
  @media (max-width: 991px) {
    white-space: initial;
  }
`;

const Div12 = styled.div`
  color: #333;
  text-transform: capitalize;
  align-self: stretch;
  flex-grow: 1;
  white-space: nowrap;
  font: 500 22px Inter, sans-serif;
  @media (max-width: 991px) {
    white-space: initial;
  }
`;

const Div13 = styled.div`
  position: relative;
  color: #666;
  text-transform: capitalize;
  align-self: stretch;
  margin-top: 9px;
  font: 400 16px/125% Inter, sans-serif;
  @media (max-width: 991px) {
    max-width: 100%;
  }
`;

const Img7 = styled.img`
  aspect-ratio: 736;
  object-fit: contain;
  object-position: center;
  width: 100%;
  stroke-width: 1px;
  stroke: #eee;
  overflow: hidden;
  align-self: stretch;
  margin-top: 20px;
  @media (max-width: 991px) {
    max-width: 100%;
  }
`;

const Div14 = styled.div`
  position: relative;
  align-self: start;
  display: flex;
  margin-top: 19px;
  gap: 12px;
`;

const Img8 = styled.img`
  aspect-ratio: 1;
  object-fit: contain;
  object-position: center;
  width: 24px;
  overflow: hidden;
  max-width: 100%;
`;

const Div15 = styled.div`
  color: #666;
  text-transform: capitalize;
  align-self: center;
  flex-grow: 1;
  white-space: nowrap;
  margin: auto 0;
  font: 500 16px Inter, sans-serif;
  @media (max-width: 991px) {
    white-space: initial;
  }
`;

const Div16 = styled.div`
  position: relative;
  align-self: start;
  display: flex;
  margin-top: 14px;
  gap: 15px;
`;

const Img9 = styled.img`
  aspect-ratio: 1;
  object-fit: contain;
  object-position: center;
  width: 18px;
  overflow: hidden;
  max-width: 100%;
`;

const Div17 = styled.div`
  color: #666;
  text-transform: capitalize;
  align-self: center;
  margin: auto 0;
  font: 500 16px Inter, sans-serif;
`;

const Div18 = styled.div`
  position: relative;
  align-self: start;
  display: flex;
  margin-top: 12px;
  gap: 12px;
`;

const Img10 = styled.img`
  aspect-ratio: 1;
  object-fit: contain;
  object-position: center;
  width: 24px;
  overflow: hidden;
  max-width: 100%;
`;

const Div19 = styled.div`
  color: #666;
  text-transform: capitalize;
  align-self: center;
  margin: auto 0;
  font: 500 16px Inter, sans-serif;
`;

const Img11 = styled.img`
  aspect-ratio: 736;
  object-fit: contain;
  object-position: center;
  width: 100%;
  stroke-width: 1px;
  stroke: #eee;
  overflow: hidden;
  align-self: stretch;
  margin-top: 18px;
  @media (max-width: 991px) {
    max-width: 100%;
  }
`;

const Div20 = styled.div`
  position: relative;
  align-self: stretch;
  display: flex;
  margin-top: 19px;
  width: 100%;
  justify-content: space-between;
  gap: 20px;
  @media (max-width: 991px) {
    max-width: 100%;
    flex-wrap: wrap;
  }
`;

const Div21 = styled.div`
  display: flex;
  justify-content: space-between;
  gap: 12px;
`;

const Img12 = styled.img`
  aspect-ratio: 1;
  object-fit: contain;
  object-position: center;
  width: 20px;
  overflow: hidden;
  max-width: 100%;
`;

const Div22 = styled.div`
  color: #666;
  text-transform: capitalize;
  align-self: start;
  flex-grow: 1;
  white-space: nowrap;
  font: 500 16px Inter, sans-serif;
  @media (max-width: 991px) {
    white-space: initial;
  }
`;

const Div23 = styled.div`
  color: #333;
  text-transform: capitalize;
  align-self: center;
  margin: auto 0;
  font: 500 22px Inter, sans-serif;
`;

const Div24 = styled.div`
  align-self: center;
  display: flex;
  margin-top: 23px;
  width: 727px;
  max-width: 100%;
  justify-content: space-between;
  gap: 20px;
  padding: 0 20px;
  @media (max-width: 991px) {
    flex-wrap: wrap;
  }
`;

const Div25 = styled.div`
  color: #666;
  text-transform: capitalize;
  font: 500 16px Inter, sans-serif;
`;

const Div26 = styled.div`
  display: flex;
  align-items: start;
  justify-content: space-between;
  gap: 4px;
`;

const Div27 = styled.div`
  color: #333;
  text-transform: capitalize;
  white-space: nowrap;
  font: 600 12px Inter, sans-serif;
  @media (max-width: 991px) {
    white-space: initial;
  }
`;

const Div28 = styled.div`
  color: #333;
  text-transform: capitalize;
  align-self: stretch;
  flex-grow: 1;
  white-space: nowrap;
  font: 500 16px Inter, sans-serif;
  @media (max-width: 991px) {
    white-space: initial;
  }
`;

const Div29 = styled.div`
  align-self: center;
  display: flex;
  margin-top: 23px;
  width: 727px;
  max-width: 100%;
  justify-content: space-between;
  gap: 20px;
  padding: 0 20px;
  @media (max-width: 991px) {
    flex-wrap: wrap;
  }
`;

const Div30 = styled.div`
  color: #666;
  text-transform: capitalize;
  font: 500 16px Inter, sans-serif;
`;

const Div31 = styled.div`
  align-self: start;
  display: flex;
  align-items: start;
  gap: 3px;
`;

const Div32 = styled.div`
  color: #333;
  text-transform: capitalize;
  white-space: nowrap;
  font: 600 12px Inter, sans-serif;
  @media (max-width: 991px) {
    white-space: initial;
  }
`;

const Div33 = styled.div`
  color: #333;
  text-transform: capitalize;
  align-self: stretch;
  flex-grow: 1;
  white-space: nowrap;
  font: 500 16px Inter, sans-serif;
  @media (max-width: 991px) {
    white-space: initial;
  }
`;

const Img13 = styled.img`
  aspect-ratio: 736;
  object-fit: contain;
  object-position: center;
  width: 736px;
  stroke-width: 1px;
  stroke: #eee;
  overflow: hidden;
  align-self: center;
  margin-top: 17px;
  max-width: 100%;
`;

const Div34 = styled.div`
  align-self: center;
  display: flex;
  margin-top: 16px;
  width: 727px;
  max-width: 100%;
  align-items: start;
  justify-content: space-between;
  gap: 20px;
  padding: 0 20px;
  @media (max-width: 991px) {
    flex-wrap: wrap;
  }
`;

const Div35 = styled.div`
  color: #666;
  text-transform: capitalize;
  font: 500 20px Inter, sans-serif;
`;

const Div36 = styled.div`
  display: flex;
  align-items: start;
  gap: 4px;
`;

const Div37 = styled.div`
  color: #333;
  text-transform: capitalize;
  white-space: nowrap;
  font: 600 12px Inter, sans-serif;
  @media (max-width: 991px) {
    white-space: initial;
  }
`;

const Div38 = styled.div`
  color: #333;
  text-transform: capitalize;
  align-self: stretch;
  flex-grow: 1;
  white-space: nowrap;
  font: 500 22px Inter, sans-serif;
  @media (max-width: 991px) {
    white-space: initial;
  }
`;

const Div39 = styled.div`
  box-shadow: 0px -2px 4px 0px rgba(0, 0, 0, 0.08);
  background-color: #fff;
  align-self: stretch;
  display: flex;
  margin-top: 25px;
  width: 100%;
  flex-direction: column;
  justify-content: center;
  align-items: center;
  padding: 16px 60px;
  @media (max-width: 991px) {
    max-width: 100%;
    padding: 0 20px;
  }
`;

const LongButton = styled.div`
  disply: flex;
  flex-direction: column;
  color: #fff;
  text-align:center;
  text-transform: capitalize;
  position: relative;
  white-space: nowrap;
  fill: var(--CTA, #0a84ff);
  overflow: hidden;
  min-height: 48px;
  width: 776px;
  max-width: 100%;
  justify-content: center;
  align-items: center;
  padding: 18px 60px;
  font: 700 17px Inter, sans-serif;
  z-index:111;
  
  @media (max-width: 991px) {
    white-space: initial;
    padding: 0 20px;
  }
`;

const ButtonImage = styled.img`
  position: absolute;
  inset: 0;
  height: 100%;
  width: 100%;
  object-fit: cover;
  object-position: center;
  z-index:-22
`;

const Div41 = styled.div`
  position: relative;
`;


export default BookingCheckout;