import * as React from "react";
import styled from "styled-components";
import Layout from "../../components/layout";
// import {Logo} from "../../assets/paymentSectionLogo.png"
function BookingPayment() {
  return (
    <Layout>
      <div className="ambuj" style={{backgroundColor:"green"}}>

      <MainDiv style={{backgroundColor:"green"}}>
        <Img
          loading="lazy"
          srcSet="/booking/paymentSectionLogo.png"
          alt="Payment Section Logo"

        />
        <PreMainDiv>
          <Img2
            loading="lazy"
            src="https://cdn.builder.io/api/v1/image/assets/TEMP/2b3be45a3d1ac20c9548f2c21e9abf981caaed0df01c8483909c53e814838b81?apiKey=9aef8ecf740c47caa7855ca5ffbd8b31&"
          />
          <Div3>
            <Div4>
              <Div5>Yoga & Meditation : Theme Based (35-80 years)</Div5>
              <Div6>
                <Div7>3</Div7>
                <Div8>Tickets</Div8>
              </Div6>
            </Div4>
            <Div9>
              <Div10>
                <Img3
                  loading="lazy"
                  srcSet="https://cdn.builder.io/api/v1/image/assets/TEMP/5e4425abf86ebba0562ceeb4784b012782735a39064bf15b3d3ec1ad1988d2c7?apiKey=9aef8ecf740c47caa7855ca5ffbd8b31&width=100 100w, https://cdn.builder.io/api/v1/image/assets/TEMP/5e4425abf86ebba0562ceeb4784b012782735a39064bf15b3d3ec1ad1988d2c7?apiKey=9aef8ecf740c47caa7855ca5ffbd8b31&width=200 200w, https://cdn.builder.io/api/v1/image/assets/TEMP/5e4425abf86ebba0562ceeb4784b012782735a39064bf15b3d3ec1ad1988d2c7?apiKey=9aef8ecf740c47caa7855ca5ffbd8b31&width=400 400w, https://cdn.builder.io/api/v1/image/assets/TEMP/5e4425abf86ebba0562ceeb4784b012782735a39064bf15b3d3ec1ad1988d2c7?apiKey=9aef8ecf740c47caa7855ca5ffbd8b31&width=800 800w, https://cdn.builder.io/api/v1/image/assets/TEMP/5e4425abf86ebba0562ceeb4784b012782735a39064bf15b3d3ec1ad1988d2c7?apiKey=9aef8ecf740c47caa7855ca5ffbd8b31&width=1200 1200w, https://cdn.builder.io/api/v1/image/assets/TEMP/5e4425abf86ebba0562ceeb4784b012782735a39064bf15b3d3ec1ad1988d2c7?apiKey=9aef8ecf740c47caa7855ca5ffbd8b31&width=1600 1600w, https://cdn.builder.io/api/v1/image/assets/TEMP/5e4425abf86ebba0562ceeb4784b012782735a39064bf15b3d3ec1ad1988d2c7?apiKey=9aef8ecf740c47caa7855ca5ffbd8b31&width=2000 2000w, https://cdn.builder.io/api/v1/image/assets/TEMP/5e4425abf86ebba0562ceeb4784b012782735a39064bf15b3d3ec1ad1988d2c7?apiKey=9aef8ecf740c47caa7855ca5ffbd8b31&"
                />
                <Img4
                  loading="lazy"
                  src="https://cdn.builder.io/api/v1/image/assets/TEMP/c589f5f745895a6f2efc88b39fd9ad1aae1a3e884395900c469a2d0b2f1cb3de?apiKey=9aef8ecf740c47caa7855ca5ffbd8b31&"
                />
              </Div10>
              <Div11>
                <Div12>Sun, 24, Apr, 2022 | 04:00 PM</Div12>
                <Div13>Online</Div13>
              </Div11>
            </Div9>
            <Div14>
              <Img5
                loading="lazy"
                srcSet="https://cdn.builder.io/api/v1/image/assets/TEMP/1954451c495067510fd44300a64a96c764df7f8cf4de7edf6de7589c746815f5?apiKey=9aef8ecf740c47caa7855ca5ffbd8b31&width=100 100w, https://cdn.builder.io/api/v1/image/assets/TEMP/1954451c495067510fd44300a64a96c764df7f8cf4de7edf6de7589c746815f5?apiKey=9aef8ecf740c47caa7855ca5ffbd8b31&width=200 200w, https://cdn.builder.io/api/v1/image/assets/TEMP/1954451c495067510fd44300a64a96c764df7f8cf4de7edf6de7589c746815f5?apiKey=9aef8ecf740c47caa7855ca5ffbd8b31&width=400 400w, https://cdn.builder.io/api/v1/image/assets/TEMP/1954451c495067510fd44300a64a96c764df7f8cf4de7edf6de7589c746815f5?apiKey=9aef8ecf740c47caa7855ca5ffbd8b31&width=800 800w, https://cdn.builder.io/api/v1/image/assets/TEMP/1954451c495067510fd44300a64a96c764df7f8cf4de7edf6de7589c746815f5?apiKey=9aef8ecf740c47caa7855ca5ffbd8b31&width=1200 1200w, https://cdn.builder.io/api/v1/image/assets/TEMP/1954451c495067510fd44300a64a96c764df7f8cf4de7edf6de7589c746815f5?apiKey=9aef8ecf740c47caa7855ca5ffbd8b31&width=1600 1600w, https://cdn.builder.io/api/v1/image/assets/TEMP/1954451c495067510fd44300a64a96c764df7f8cf4de7edf6de7589c746815f5?apiKey=9aef8ecf740c47caa7855ca5ffbd8b31&width=2000 2000w, https://cdn.builder.io/api/v1/image/assets/TEMP/1954451c495067510fd44300a64a96c764df7f8cf4de7edf6de7589c746815f5?apiKey=9aef8ecf740c47caa7855ca5ffbd8b31&"
              />
              <Div15>Zoom</Div15>
            </Div14>
            <Img6
              loading="lazy"
              src="https://cdn.builder.io/api/v1/image/assets/TEMP/8f4c23cde3cf9ce255d7d24e1d86903211d0e3a6bde2a22da353bb7e47cac168?apiKey=9aef8ecf740c47caa7855ca5ffbd8b31&"
            />
            <Div16>
              <Div17>
                <Div18>Sub-total</Div18>
                <Div19>
                  <Div20>Concenience Fee</Div20>
                  <Img7
                    loading="lazy"
                    src="https://cdn.builder.io/api/v1/image/assets/TEMP/254339caa5da63ce697cb1f51dcd9cdec7fcfdf54dfb92f9e3740236a4e796cf?apiKey=9aef8ecf740c47caa7855ca5ffbd8b31&"
                  />
                </Div19>
                <Div21>Total Amount</Div21>
              </Div17>
              <Div22>
                <Div23>
                  <Div24>₹</Div24>
                  <Div25>450.00</Div25>
                </Div23>
                <Div26>
                  <Div27>₹</Div27>
                  <Div28>20.00</Div28>
                </Div26>
                <Div29>
                  <Div30>₹</Div30>
                  <Div31>470.00</Div31>
                </Div29>
              </Div22>
            </Div16>
            <Img8
              loading="lazy"
              src="https://cdn.builder.io/api/v1/image/assets/TEMP/8f4c23cde3cf9ce255d7d24e1d86903211d0e3a6bde2a22da353bb7e47cac168?apiKey=9aef8ecf740c47caa7855ca5ffbd8b31&"
            />
            <Div32>
              <Img9
                loading="lazy"
                src="https://cdn.builder.io/api/v1/image/assets/TEMP/0c8ff3bd53478b72fec99592bea1cf08253960c48fa749aff9d04b2b152a3900?apiKey=9aef8ecf740c47caa7855ca5ffbd8b31&"
              />
              <Div33>
                <Img10
                  loading="lazy"
                  alt="groupStar"
                  src="/booking/Group2379.png"
                />
                <Div34>
                  <Div35>You have earned </Div35>
                  <Div36>125 Care Points</Div36>
                </Div34>
                <StarDance
                  loading="lazy"
                  srcSet="/booking/image17139.png"
                  alt="booking"
                />
              </Div33>
            </Div32>
            <Div37>
              mansuri.wasim@gmail.com
              <br />
              9930391551 | Maharashtra
            </Div37>
            <Img12 loading="lazy" src="/booking/image 17139.png" />
            <Div38>
              Abdul Rashid Mansuri
              <br />
              9930391551 | Maharashtra
            </Div38>
            <Img12 loading="lazy" src="/booking/image 17139.png" />
            <Div39>
              Parveen Mansuri
              <br />
              9930391551 | Maharashtra
            </Div39>
          </Div3>
          <Img14
            loading="lazy"
            src="https://cdn.builder.io/api/v1/image/assets/TEMP/668770b94f1674d05cc8eabb57f334db2d73dc233bf1d2fbbfb38fd80878bef6?apiKey=9aef8ecf740c47caa7855ca5ffbd8b31&"
          />
          <Div40>
            <Img15 loading="lazy" srcSet="/booking/image38.png" />
            <Div41>
              <Img16
                loading="lazy"
                src="https://cdn.builder.io/api/v1/image/assets/TEMP/5b9eeaf13eb88455c4c75c2754201c3578d93611bce44d8c63aae5239a3d1307?apiKey=9aef8ecf740c47caa7855ca5ffbd8b31&"
              />
              Resend Confirmation
            </Div41>
            <Div43>
              <Img17
                loading="lazy"
                src="https://cdn.builder.io/api/v1/image/assets/TEMP/5b9eeaf13eb88455c4c75c2754201c3578d93611bce44d8c63aae5239a3d1307?apiKey=9aef8ecf740c47caa7855ca5ffbd8b31&"
              />
              Invite Friends
            </Div43>
          </Div40>
        </PreMainDiv>
      </MainDiv>
      </div>
    </Layout>
  );
}
export default BookingPayment;
const MainDiv = styled.div`
  display: flex;
  max-width: 776px;
  flex-direction: column;
  align-items: center;
  background-color: #fff !important;
`;

const Img = styled.img`
  ${'' /* margin-top:60px; */}
  aspect-ratio: 1.9;
  object-fit: contain;
  object-position: center;
  width: 200px;
  mix-blend-mode: darken;
  overflow: hidden;
  z-index: 111;
  max-width: 100%;
`;

const PreMainDiv = styled.div`
  background-color: #FFFFFF;
  disply: flex;
  flex-direction: row;
  fill: #fff;
  overflow: hidden;
  align-self: stretch;
  position: relative;
  display: flex;
  min-height: 620px;
  justify-content: space-between;
  gap: 20px;
  z-index:-141px;
  padding: 42px 24px 16px 33px;
  @media (max-width: 991px) {
    max-width: 100%;
    flex-wrap: wrap;
    padding: 0 20px;
  }
`;

const Img2 = styled.img`
  position: absolute;
  inset: 0;
  height: 100%;
  width: 100%;
  object-fit: cover;
  object-position: center;
`;

const Div3 = styled.div`
  position: relative;
  align-self: start;
  display: flex;
  margin-top: 20px;
  flex-grow: 1;
  flex-basis: 0%;
  flex-direction: column;
`;

const Div4 = styled.div`
  align-self: stretch;
  display: flex;
  justify-content: space-between;
  gap: 20px;
`;

const Div5 = styled.div`
  color: #333;
  text-transform: capitalize;
  flex-grow: 1;
  flex-basis: auto;
  font: 600 18px/24px Inter, sans-serif;
`;

const Div6 = styled.div`
  align-self: start;
  display: flex;
  flex-basis: 0%;
  flex-direction: column;
`;

const Div7 = styled.div`
  color: #333;
  text-transform: capitalize;
  white-space: nowrap;
  font: 600 20px/90% Inter, sans-serif;
  @media (max-width: 991px) {
    white-space: initial;
  }
`;

const Div8 = styled.div`
  color: #666;
  text-transform: capitalize;
  margin-top: 9px;
  white-space: nowrap;
  font: 400 12px/167% Inter, sans-serif;
  @media (max-width: 991px) {
    white-space: initial;
  }
`;

const Div9 = styled.div`
  align-self: start;
  display: flex;
  margin-top: 14px;
  gap: 8px;
`;

const Div10 = styled.div`
  display: flex;
  flex-basis: 0%;
  flex-direction: column;
  align-items: center;
`;

const Img3 = styled.img`
  aspect-ratio: 1;
  object-fit: contain;
  object-position: center;
  width: 18px;
  overflow: hidden;
`;

const Img4 = styled.img`
  aspect-ratio: 1.29;
  object-fit: contain;
  object-position: center;
  width: 18px;
  overflow: hidden;
  margin-top: 14px;
`;

const Div11 = styled.div`
  align-self: start;
  display: flex;
  flex-grow: 1;
  flex-basis: 0%;
  flex-direction: column;
`;

const Div12 = styled.div`
  color: #333;
  text-transform: capitalize;
  white-space: nowrap;
  font: 400 12px/150% Inter, sans-serif;
  @media (max-width: 991px) {
    white-space: initial;
  }
`;

const Div13 = styled.div`
  color: #333;
  text-transform: capitalize;
  margin-top: 18px;
  font: 400 12px/150% Inter, sans-serif;
`;

const Div14 = styled.div`
  align-self: start;
  display: flex;
  margin-top: 14px;
  width: 57px;
  max-width: 100%;
  gap: 8px;
`;

const Img5 = styled.img`
  aspect-ratio: 1;
  object-fit: contain;
  object-position: center;
  width: 100%;
  overflow: hidden;
  flex: 1;
`;

const Div15 = styled.div`
  color: #333;
  text-transform: capitalize;
  align-self: center;
  margin: auto 0;
  font: 400 12px/150% Inter, sans-serif;
`;

const Img6 = styled.img`
  aspect-ratio: 206.5;
  object-fit: contain;
  object-position: center;
  width: 100%;
  stroke-width: 1px;
  stroke: #eee;
  overflow: hidden;
  align-self: stretch;
  margin-top: 15px;
`;

const Div16 = styled.div`
  align-self: stretch;
  display: flex;
  margin-top: 20px;
  padding-right: 20px;
  align-items: start;
  justify-content: space-between;
  gap: 20px;
`;

const Div17 = styled.div`
  display: flex;
  flex-direction: column;
`;

const Div18 = styled.div`
  color: #333;
  font: 400 12px/150% Inter, sans-serif;
`;

const Div19 = styled.div`
  display: flex;
  margin-top: 22px;
  justify-content: space-between;
  gap: 6px;
`;

const Div20 = styled.div`
  color: #333;
  flex-grow: 1;
  white-space: nowrap;
  font: 400 12px/150% Inter, sans-serif;
  @media (max-width: 991px) {
    white-space: initial;
  }
`;

const Img7 = styled.img`
  aspect-ratio: 1.75;
  object-fit: contain;
  object-position: center;
  width: 7px;
  stroke-width: 1px;
  stroke: #333;
  overflow: hidden;
  align-self: start;
  margin-top: 5px;
  max-width: 100%;
`;

const Div21 = styled.div`
  color: #333;
  margin-top: 22px;
  white-space: nowrap;
  font: 400 13px/138% Inter, sans-serif;
  @media (max-width: 991px) {
    white-space: initial;
  }
`;

const Div22 = styled.div`
  align-self: stretch;
  display: flex;
  flex-direction: column;
`;

const Div23 = styled.div`
  display: flex;
  align-items: start;
  justify-content: space-between;
  gap: 3px;
`;

const Div24 = styled.div`
  color: #666;
  text-transform: capitalize;
  white-space: nowrap;
  font: 600 8px Inter, sans-serif;
  @media (max-width: 991px) {
    white-space: initial;
  }
`;

const Div25 = styled.div`
  color: #333;
  text-align: right;
  text-transform: capitalize;
  align-self: stretch;
  flex-grow: 1;
  white-space: nowrap;
  font: 600 13px/154% Inter, sans-serif;
  @media (max-width: 991px) {
    white-space: initial;
  }
`;

const Div26 = styled.div`
  display: flex;
  margin-top: 16px;
  align-items: start;
  justify-content: space-between;
  gap: 4px;
`;

const Div27 = styled.div`
  color: #666;
  text-transform: capitalize;
  white-space: nowrap;
  font: 600 8px Inter, sans-serif;
  @media (max-width: 991px) {
    white-space: initial;
  }
`;

const Div28 = styled.div`
  color: #333;
  text-align: right;
  text-transform: capitalize;
  align-self: stretch;
  flex-grow: 1;
  white-space: nowrap;
  font: 600 13px/154% Inter, sans-serif;
  @media (max-width: 991px) {
    white-space: initial;
  }
`;

const Div29 = styled.div`
  display: flex;
  margin-top: 26px;
  align-items: start;
  justify-content: space-between;
  gap: 2px;
`;

const Div30 = styled.div`
  color: #666;
  text-transform: capitalize;
  white-space: nowrap;
  font: 600 8px Inter, sans-serif;
  @media (max-width: 991px) {
    white-space: initial;
  }
`;

const Div31 = styled.div`
  color: #333;
  text-align: right;
  text-transform: capitalize;
  align-self: stretch;
  flex-grow: 1;
  white-space: nowrap;
  font: 600 13px/154% Inter, sans-serif;
  @media (max-width: 991px) {
    white-space: initial;
  }
`;

const Img8 = styled.img`
  aspect-ratio: 206.5;
  object-fit: contain;
  object-position: center;
  width: 100%;
  stroke-width: 1px;
  stroke: #eee;
  overflow: hidden;
  align-self: stretch;
  margin-top: 18px;
`;

const Div32 = styled.div`
  disply: flex;
  flex-direction: column;
  fill: #fff9f2;
  stroke-width: 1px;
  stroke: #eccca7;
  overflow: hidden;
  align-self: center;
  position: relative;
  display: flex;
  aspect-ratio: 6.257575757575758;
  margin-top: 19px;
  width: 100%;
  align-items: center;
  justify-content: space-between;
  gap: 20px;
  padding: 4px 10px;
`;

const Img9 = styled.img`
  position: absolute;
  inset: 0;
  height: 110%;
  width: 100%;
  object-fit: cover;
  object-position: center;
`;

const Div33 = styled.div`
  position: relative;
  display: flex;
  gap: 8px;
  margin: auto 0;
`;

const Img10 = styled.img`
  aspect-ratio: 1;
  object-fit: contain;
  object-position: center;
  width: 34px;
  overflow: hidden;
  max-width: 100%;
`;

const Div34 = styled.div`
  align-self: start;
  display: flex;
  flex-grow: 1;
  flex-basis: 0%;
  flex-direction: column;
`;

const Div35 = styled.div`
  color: #333;
  font: 400 12px/100% Inter, sans-serif;
`;

const Div36 = styled.div`
  color: #333;
  margin-top: 8px;
  white-space: nowrap;
  font: 800 18px/100% Inter, sans-serif;
  @media (max-width: 991px) {
    white-space: initial;
  }
`;

const StarDance = styled.img`
  aspect-ratio: 2.2;
  object-fit: contain;
  object-position: center;
  width: 120px;
  overflow: hidden;
  align-self: stretch;
  max-width: 80%;
`;

const Div37 = styled.div`
  color: #666;
  align-self: stretch;
  margin-top: 25px;
  font: 400 12px/20px Inter, sans-serif;
`;

const Img12 = styled.img`
  aspect-ratio: 413;
  object-fit: contain;
  object-position: center;
  width: 100%;
  stroke-width: 1px;
  stroke: #eee;
  overflow: hidden;
  align-self: stretch;
  margin-top: 16px;
`;

const Div38 = styled.div`
  color: #666;
  align-self: stretch;
  margin-top: 15px;
  font: 400 12px/20px Inter, sans-serif;
`;

const Img13 = styled.img`
  aspect-ratio: 413;
  object-fit: contain;
  object-position: center;
  width: 100%;
  stroke-width: 1px;
  stroke: #eee;
  overflow: hidden;
  align-self: stretch;
  margin-top: 16px;
`;

const Div39 = styled.div`
  color: #666;
  align-self: stretch;
  margin-top: 15px;
  font: 400 12px/20px Inter, sans-serif;
`;

const Img14 = styled.img`
  aspect-ratio: 0;
  object-fit: contain;
  object-position: center;
  width: 1px;
  stroke-width: 1px;
  stroke: #eee;
  overflow: hidden;
  max-width: 100%;
`;

const Div40 = styled.div`
  position: relative;
  align-self: start;
  display: flex;
  margin-top: 52px;
  flex-grow: 1;
  flex-basis: 0%;
  flex-direction: column;
  align-items: start;
  @media (max-width: 991px) {
    margin-top: 40px;
  }
`;

const Img15 = styled.img`
  padding-left:50px !important;
  aspect-ratio: 1.18;
  object-fit: contain;
  object-position: center;
  width: 206px;
  overflow: hidden;
  max-width: 100%;
`;

const Div41 = styled.div`
  z-index: 111;
  disply: flex;
  flex-direction: column;
  color: #333;
  text-align: center;
  text-transform: capitalize;
  position: relative;
  white-space: nowrap;
  fill: #fff;
  stroke-width: 1px;
  stroke: #d8d8d8;
  overflow: hidden;
  align-self: center;
  aspect-ratio: 6.15;
  margin-top: 55px;
  justify-content: center;
  padding: 15px 54px;
  font: 500 14px/100% Inter, sans-serif;
  @media (max-width: 991px) {
    white-space: initial;
    margin-top: 40px;
    padding: 0 20px;
  }
`;

const Img16 = styled.img`
  position: absolute;
  z-index: -111;
  inset: 0;
  height: 100%;
  width: 100%;
  object-fit: cover;
  object-position: center;
`;

const Div42 = styled.div`
  position: relative;
`;

const Div43 = styled.div`
  z-index: 111;
  disply: flex;
  flex-direction: column;
  color: #333;
  text-align: center;
  text-transform: capitalize;
  position: relative;
  white-space: nowrap;
  fill: #fff;
  stroke-width: 1px;
  stroke: #d8d8d8;
  overflow: hidden;
  align-self: center;
  aspect-ratio: 6.15;
  margin-top: 12px;
  width: 246px;
  justify-content: center;
  align-items: center;
  padding: 15px 60px;
  font: 500 14px/100% Inter, sans-serif;
  @media (max-width: 991px) {
    white-space: initial;
    padding: 0 20px;
  }
`;

const Img17 = styled.img`
  z-index: -111;

  position: absolute;
  inset: 0;
  height: 100%;
  width: 100%;
  object-fit: cover;
  object-position: center;
`;

const Div44 = styled.div`
  position: relative;
`;
