import React from 'react';
import Layout from '../../../components/layout';
import {
    VStack,
    Stack,
    Box,
} from '@chakra-ui/react'

import EventDetails from '../../../components/details/events-details';
import EventsBooking from '../../../components/booking/events';

const EventsPage = () => {
    return <VStack>
        <Layout showBanner={false}>
           <Stack>
                <EventsBooking />
           </Stack>
        </Layout>
    </VStack>
}

export default EventsPage;