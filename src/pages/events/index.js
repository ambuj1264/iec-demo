import React from "react";
import Layout from "../../components/layout";
import { VStack, Stack, Box } from "@chakra-ui/react";

import EventListing from "../../components/list/events-lists";

const EventsPage = () => {
  return (
    <VStack>
      <Layout showBanner={true}>
        <Stack>
          <EventListing />
        </Stack>
      </Layout>
    </VStack>
  );
};

export default EventsPage;
