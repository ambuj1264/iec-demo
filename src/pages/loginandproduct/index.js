import React from 'react'
import {
  Accordion,
  AccordionItem,
  AccordionButton,
  AccordionPanel,
  AccordionIcon,
  Box,
} from '@chakra-ui/react';
import { Divider } from '@chakra-ui/react'
import { Avatar } from '@chakra-ui/react';
import styled from 'styled-components'
import Layout from '../../components/layout'
import OrderSummary from '../../components/loginandproduct/OrderSummary';
import Login from '../../components/loginandproduct/Login';
import Ticket from '../../components/loginandproduct/Ticket';
const Loginanddata = () => {
  return (
    <>
      <Layout showBanner={false}>
        <Wrapper >
          <CenterDiv >
            <Accordion allowToggle  >
              <AccordionItem >
                <h2>
                  <AccordionButton borderRadius="15px"  >
                    <Box as="span" flex='1' textAlign='left' >
                     
                    <LoginSection>  
                    <span> <Avatar
                        size="sm"
                        name="1"
                        color="black"
                        bg="#fff"
                        border="0.5px  solid #f2f2f2"
                      /><Heading>Logged in as:</Heading> </span>
                    <Details> mansuri.wasim@gmail.com</Details>
                    </LoginSection>  
                    </Box>
                    <AccordionIcon />
                  </AccordionButton>
                </h2>
                <Divider />
                <AccordionPanel pb={4} px={10}>
                <Login/>

                </AccordionPanel>
              </AccordionItem>
              <AccordionItem >
                <h2>
                  <AccordionButton borderRadius="15px" >
                    <Box as="span" flex='1' textAlign='left' >
                    <Avatar
                        size="sm"
                        name="2"
                        color="black"
                        bg="#fff"
                        border="0.5px  solid #f2f2f2"
                      />
                      
                      <Heading> Order Summary</Heading>
                    </Box>
                    <AccordionIcon />
                  </AccordionButton>
                </h2>
                <AccordionPanel pb={4}>
                <OrderSummary/>
                </AccordionPanel>
              </AccordionItem>

              <AccordionItem borderRadius="15px" >
                <h2>
                  <AccordionButton>
                    <Box as="span" flex='1' textAlign='left'>
                    <Avatar
                        size="sm"
                        name="3"
                        color="black"
                        bg="#fff"
                        border="0.5px  solid #f2f2f2"
                      />
                      <Heading> Ticket Details</Heading>
                    </Box>
                    <AccordionIcon />
                  </AccordionButton>
                </h2>
                <AccordionPanel pb={4} >
                <Ticket/>
                </AccordionPanel>
              </AccordionItem>
            </Accordion>
          </CenterDiv>
        </Wrapper>
      </Layout>
    </>
  )
}

export default Loginanddata;

const Wrapper = styled.div`
  width: 100%;
  background-color: #f3f3f3 !important;
  display:flex;
  justify-content:center; 
  align-items: center;
`
const CenterDiv = styled.div`
    width:776px;
    background-color: #fff !important;
    border-radius: 15px;
    border: none;
     .css-186l2rg 
      {
      display: none !important;
  }
`

const Heading = styled.span`
  padding-left: 20px;

`
const Details = styled.span`
  display:flex;
  
`

const   LoginSection= styled.div`
    display:flex;
    justify-content:space-between;
`
