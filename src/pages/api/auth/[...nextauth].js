import NextAuth from "next-auth";
import CredentialProvider from "next-auth/providers/credentials";
import GoogleProvider from "next-auth/providers/google";
// import {CredentialsProvider, Google} from 'next-auth/providers'
// import Providers from 'next-auth/providers';
// export default NextAuth({
export const authOptions = {
  session: {
    strategy: "jwt",
  },
  providers: [
    CredentialProvider({
      async authorize(credentials) {
        const baseUrl = `${process.env.NEXT_PUBLIC_API_ENDPOINT}/`;
        const response = await fetch(
          baseUrl + "auth/login",
          {
            method: "POST",
            body: JSON.stringify(credentials),
            headers: {
              "Content-Type": "application/json",
            },
          }
        );
        const data = await response.json();
        console.log('data', data)
        if (data?.error) {
          return null
        }
        // Returning token to set in session
        return {
          token: data,
        };
      }
    }),
    GoogleProvider({
      clientId: process.env.GOOGLE_CLIENT_ID,
      clientSecret: process.env.GOOGLE_CLIENT_SECRET,
    }),
  ],
  
  secret: process.env.NEXT_PUBLIC_NEXTAUTH_SECRET,
  callbacks: {
   
    jwt: async ({ token, user }) => {
      user && (token.user = user);
      return token;
    },
    session: async ({ session, token, user }) => {
      session.user = token.user;  // Setting token in session
      return session;
    },
  },
  pages: {
    signIn: "/login", //Need to define custom login page (if using)
    signOut: "/",
  },
};

export default NextAuth(authOptions);