// import bcrypt from "bcrypt";

// import prisma from "../../../lib/prismadb";
// import verifyResetToken from "../../../lib/verifyResetToken";
// import sendMail from "../../../lib/sendMail";
// import { createPasswordSuccess } from "../../../templates/earlyAccess";

// export default async function handler(req, res) {
//   if (req.method === "PUT") {
//     const result = await verifyResetToken(req.body.token);
//     try {
//       if (result?.success) {
//         const hashedPassword = await bcrypt.hash(req.body.password, 10);

//         const updatedUser = await prisma.user.update({
//           where: {
//             email: result?.email,
//           },
//           data: {
//             password: hashedPassword,
//             passwordResetToken: null,
//           },
//         });

//         if (updatedUser) {
//           try {
//             await sendMail({
//               to: result?.email,
//               subject: "Welcome to Grootin!",
//               html: createPasswordSuccess(result?.name),
//             });
//           } catch (error) {
//             console.log(error);
//           }
//           return res.json({
//             success: true,
//           });
//         }

//         return res.json({ success: false, message: "Something went wrong" });
//       } else {
//         res.json({
//           success: false,
//           message: result.message,
//         });
//       }
//     } catch (error) {
//       console.log(error);
//       return res.json({
//         success: false,
//         message: `Error while creating password`,
//       });
//     }
//   }

//   return res.status(405).send("method not allowed");
// }
