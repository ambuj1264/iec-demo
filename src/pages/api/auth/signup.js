// import { Role } from "@prisma/client";
// import { generateFromEmail } from "unique-username-generator";

// import prisma from "../../../lib/prismadb";
// import sendMail from "../../../lib/sendMail";
// import excludeFields from "../../../lib/excludeFields";
// import { waitList } from "../../../templates/earlyAccess";

// export default async function handle(req, res) {
//   if (req.method === "POST") {
//     let newUser = {};
//     const { role, email, name} = req.body;

//     try {
//       //check if user already exists
//         const user = await prisma.user.findUnique({
//           where: {
//             email,
//           },
//         });

//         if (user) {
//           return res.json({
//             success: false,
//             message: "User already exists. Please login",
//           });
//         }

//       newUser = await prisma.user.create({
//         data: { ...req.body, username: generateFromEmail(email, 4) },
//       });

//       if (role === Role.FAN) {
//         delete req.body.socialLinks;
//       }

//       try {
//         await sendMail({
//           to: email,
//           subject: "Welcome to Grootin!",
//           html: waitList(role,name),
//         });
//       } catch (error) {
//         console.log(error);
//       }

//       return res.json({
//         success: true,
//         data: excludeFields(newUser, ["password"]),
//       });
//     } catch (error) {
//       console.log(error);
//       return res.json({
//         success: false,
//         message: `Error while creating user`,
//       });
//     }
//   }

//   return res
//     .status(405)
//     .json({ success: false, message: "Method not allowed" });
// }
