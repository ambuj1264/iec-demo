import "@fontsource/inter";
import { Provider } from "react-redux";
import NextProgress from "next-progress";
import { ChakraProvider } from "@chakra-ui/react";
import Head from 'next/head'
import theme from "../libs/theme";
import { SessionProvider } from "next-auth/react";
import store from '../redux/store';
const App = ({ Component, pageProps: { session, ...pageProps } }) => {
  return (
    <>
      <Head>
                <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0,user-scalable=0" />
      </Head>
      <NextProgress
        height="4px"
        color="#ffffff"
        options={{ showSpinner: false }}
      />
       <SessionProvider session={session}>
        <Provider store={store}>
          <ChakraProvider resetCSS theme={theme}>
            <Component {...pageProps} />
            {/* <Analytics /> */}
          </ChakraProvider>
        </Provider>

       </SessionProvider>
      <style>{`
        .loaderLogo {
          animation: beat 0.75s alternate infinite ease-in;
        }
        
        @keyframes beat {
          0%   {transform: scale(.8);}
          100% {transform: scale(1);}
        }
      `}</style>
    </>
  );
};

export default App;
